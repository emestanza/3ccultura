<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="conferencia")
 */
class Conferencia
{
    public function __construct() {
        $this->horarios = new ArrayCollection();
    }
    
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=150)
     */
    private $nombre;

    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var Decimal $precio
     *
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=2)
     */
    private $precio;

    /**
     * @ORM\Column(name="tipo_moneda", type="string", length=5)
     */
    private $tipoMoneda;

    /**
     * @var string $fid_img
     *
     * @ORM\Column(name="fid_img", type="integer", nullable=true)
     */
    private $fid_img;

    /**
     * @var \DateTime $fecha_inicio
     *
     * @ORM\Column(name="fecha_inicio", type="datetime")
     */
    private $fecha_inicio;

    /**
     * @var \DateTime $fecha_fin
     *
     * @ORM\Column(name="fecha_fin", type="datetime")
     */
    private $fecha_fin;

    /**
     * @ORM\Column(name="duracion", type="string", length=20)
     */
    private $duracion;

    /**
     * @var string $uri
     *
     * @ORM\Column(name="uri", type="string", length=255)
     */
    private $uri;


    /**
     * @ORM\Column(type="boolean")
     */
    private $estado;

    /**
     * @var user
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Conferencista")
     * @ORM\JoinColumn(name="conferencista_id", referencedColumnName="id")
     */
    private $conferencista;


    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Conferencia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Conferencia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set precio
     *
     * @param string $precio
     * @return Conferencia
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set fid_img
     *
     * @param integer $fidImg
     * @return Conferencia
     */
    public function setFidImg($fidImg)
    {
        $this->fid_img = $fidImg;

        return $this;
    }

    /**
     * Get fid_img
     *
     * @return integer
     */
    public function getFidImg()
    {
        return $this->fid_img;
    }

    /**
     * Set fecha_inicio
     *
     * @param \DateTime $fechaInicio
     * @return Conferencia
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fecha_inicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fecha_inicio
     *
     * @return \DateTime
     */
    public function getFechaInicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * Set fecha_fin
     *
     * @param \DateTime $fechaFin
     * @return Conferencia
     */
    public function setFechaFin($fechaFin)
    {
        $this->fecha_fin = $fechaFin;

        return $this;
    }

    /**
     * Get fecha_fin
     *
     * @return \DateTime
     */
    public function getFechaFin()
    {
        return $this->fecha_fin;
    }

    /**
     * Set estado
     *
     * @param boolean $estado
     * @return Conferencia
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return boolean
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set conferencista
     *
     * @param \AppBundle\Entity\Conferencista $conferencista
     * @return Conferencia
     */
    public function setConferencista(\AppBundle\Entity\Conferencista $conferencista = null)
    {
        $this->conferencista = $conferencista;

        return $this;
    }

    /**
     * Get conferencista
     *
     * @return \AppBundle\Entity\Conferencista
     */
    public function getConferencista()
    {
        return $this->conferencista;
    }

    /**
     * Set uri
     *
     * @param string $uri
     * @return Conferencia
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set duracion
     *
     * @param \DateTime $duracion
     * @return Conferencia
     */
    public function setDuracion($duracion)
    {
        $this->duracion = $duracion;

        return $this;
    }

    /**
     * Get duracion
     *
     * @return \DateTime
     */
    public function getDuracion()
    {
        return $this->duracion;
    }

    /**
     * Set tipoMoneda
     *
     * @param string $tipoMoneda
     * @return Conferencia
     */
    public function setTipoMoneda($tipoMoneda)
    {
        $this->tipoMoneda = $tipoMoneda;

        return $this;
    }

    /**
     * Get tipoMoneda
     *
     * @return string
     */
    public function getTipoMoneda()
    {
        return $this->tipoMoneda;
    }
    
}
