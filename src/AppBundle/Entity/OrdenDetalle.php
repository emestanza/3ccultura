<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="orden_detalle")
 */
class OrdenDetalle
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Conferencia")
     * @ORM\JoinColumn(name="conferencia_id", referencedColumnName="id")
     **/
    private $conferencia;

    /**
     * @var Decimal $precio
     *
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=2)
     */
    private $precio;

    /**
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @var string $boletaNombre
     *
     * @ORM\Column(name="boleta_nombre", type="string", length=50, nullable=true)
     */
    private $boletaNombre;

    /**
     * @var string $boletaDni
     *
     * @ORM\Column(name="boleta_dni", type="string", length=10, nullable=true)
     */
    private $boletaDni;

    /**
     * @var string $facturaRazonSocial
     *
     * @ORM\Column(name="factura_razon_social", type="string", length=150, nullable=true)
     */
    private $facturaRazonSocial;

    /**
     * @var string $facturaRuc
     *
     * @ORM\Column(name="factura_ruc", type="string", length=15, nullable=true)
     */
    private $facturaRuc;

    /**
     * @var string $facturaDireccion
     *
     * @ORM\Column(name="factura_direccion", type="text", nullable=true)
     */
    private $facturaDireccion;

    /**
     * @var string $exteriorEmpresa
     *
     * @ORM\Column(name="exterior_nombre", type="string", length=150, nullable=true)
     */
    private $exteriorEmpresa;

    /**
     * @var string $exteriorDireccion
     *
     * @ORM\Column(name="exterior_direccion", type="text", nullable=true)
     */
    private $exteriorDireccion;

    /**
     * @var string $exteriorPais
     *
     * @ORM\Column(name="exterior_país", type="string", length=3, nullable=true)
     */
    private $exteriorPais;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Orden", inversedBy="detalle")
     * @ORM\JoinColumn(name="orden_id", referencedColumnName="id")
     **/
    private $orden;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set precio
     *
     * @param string $precio
     * @return OrdenDetalle
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return OrdenDetalle
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set orden
     *
     * @param \AppBundle\Entity\Orden $orden
     * @return OrdenDetalle
     */
    public function setOrden(\AppBundle\Entity\Orden $orden = null)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return \AppBundle\Entity\Orden
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set conferencia
     *
     * @param \AppBundle\Entity\conferencia $conferencia
     * @return OrdenDetalle
     */
    public function setConferencia(\AppBundle\Entity\Conferencia $conferencia = null)
    {
        $this->conferencia = $conferencia;

        return $this;
    }

    /**
     * Get conferencia
     *
     * @return \AppBundle\Entity\Conferencia
     */
    public function getConferencia()
    {
        return $this->conferencia;
    }

    /**
     * Set boletaNombre
     *
     * @param string $boletaNombre
     * @return OrdenDetalle
     */
    public function setBoletaNombre($boletaNombre)
    {
        $this->boletaNombre = $boletaNombre;

        return $this;
    }

    /**
     * Get boletaNombre
     *
     * @return string
     */
    public function getBoletaNombre()
    {
        return $this->boletaNombre;
    }

    /**
     * Set boletaDni
     *
     * @param string $boletaDni
     * @return OrdenDetalle
     */
    public function setBoletaDni($boletaDni)
    {
        $this->boletaDni = $boletaDni;

        return $this;
    }

    /**
     * Get boletaDni
     *
     * @return string
     */
    public function getBoletaDni()
    {
        return $this->boletaDni;
    }

    /**
     * Set facturaRazonSocial
     *
     * @param string $facturaRazonSocial
     * @return OrdenDetalle
     */
    public function setFacturaRazonSocial($facturaRazonSocial)
    {
        $this->facturaRazonSocial = $facturaRazonSocial;

        return $this;
    }

    /**
     * Get facturaRazonSocial
     *
     * @return string
     */
    public function getFacturaRazonSocial()
    {
        return $this->facturaRazonSocial;
    }

    /**
     * Set facturaRuc
     *
     * @param string $facturaRuc
     * @return OrdenDetalle
     */
    public function setFacturaRuc($facturaRuc)
    {
        $this->facturaRuc = $facturaRuc;

        return $this;
    }

    /**
     * Get facturaRuc
     *
     * @return string
     */
    public function getFacturaRuc()
    {
        return $this->facturaRuc;
    }

    /**
     * Set facturaDireccion
     *
     * @param string $facturaDireccion
     * @return OrdenDetalle
     */
    public function setFacturaDireccion($facturaDireccion)
    {
        $this->facturaDireccion = $facturaDireccion;

        return $this;
    }

    /**
     * Get facturaDireccion
     *
     * @return string
     */
    public function getFacturaDireccion()
    {
        return $this->facturaDireccion;
    }

    /**
     * Set exteriorEmpresa
     *
     * @param string $exteriorEmpresa
     * @return OrdenDetalle
     */
    public function setExteriorEmpresa($exteriorEmpresa)
    {
        $this->exteriorEmpresa = $exteriorEmpresa;

        return $this;
    }

    /**
     * Get exteriorEmpresa
     *
     * @return string
     */
    public function getExteriorEmpresa()
    {
        return $this->exteriorEmpresa;
    }

    /**
     * Set exteriorDireccion
     *
     * @param string $exteriorDireccion
     * @return OrdenDetalle
     */
    public function setExteriorDireccion($exteriorDireccion)
    {
        $this->exteriorDireccion = $exteriorDireccion;

        return $this;
    }

    /**
     * Get exteriorDireccion
     *
     * @return string
     */
    public function getExteriorDireccion()
    {
        return $this->exteriorDireccion;
    }

    /**
     * Set exteriorPais
     *
     * @param string $exteriorPais
     * @return OrdenDetalle
     */
    public function setExteriorPais($exteriorPais)
    {
        $this->exteriorPais = $exteriorPais;

        return $this;
    }

    /**
     * Get exteriorPais
     *
     * @return string
     */
    public function getExteriorPais()
    {
        return $this->exteriorPais;
    }
}
