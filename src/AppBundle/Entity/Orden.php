<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="orden")
 */
class Orden
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false, unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime $fecharegistro
     *
     * @ORM\Column(name="fecha_registro", type="datetime", nullable=true)
     */
    private $fecharegistro;

    /**
     * @var \DateTime $fechavcto
     *
     * @ORM\Column(name="fecha_vencimiento", type="datetime", nullable=true)
     */
    private $fechavcto;

    /**
     * @var \DateTime $fechapago
     *
     * @ORM\Column(name="fecha_pago", type="datetime", nullable=true)
     */
    private $fechapago;

    /**
     * @var string $metodopago
     *
     * @ORM\Column(name="metodo_pago", type="string", length=15, nullable=true)
     */
    private $metodopago;

    /**
     * @var string $codigo_pago
     *
     * @ORM\Column(name="codigo_pago", type="string", length=15, nullable=true)
     */
    private $codigo_pago;

    /**
     * @var string $estado_pago
     *
     * @ORM\Column(name="estado_pago", type="string", length=50, nullable=true)
     */
    private $estado_pago;

    /**
     * @var string $tipo_moneda
     *
     * @ORM\Column(name="tipo_moneda", type="string", length=10, nullable=true)
     */
    private $tipo_moneda;

    /**
     * @var string $importe
     *
     * @ORM\Column(name="importe", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $importe;

    /**
     * @var boolean $certificado
     *
     * @ORM\Column(name="certificado", type="boolean", nullable=true)
     */
    private $certificado;

    /**
     * @var user
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrdenDetalle", mappedBy="orden")
     **/
    private $detalle;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecharegistro
     *
     * @param \DateTime $fecharegistro
     * @return Transaccion
     */
    public function setFecharegistro($fecharegistro)
    {
        $this->fecharegistro = $fecharegistro;

        return $this;
    }

    /**
     * Get fecharegistro
     *
     * @return \DateTime 
     */
    public function getFecharegistro()
    {
        return $this->fecharegistro;
    }

    /**
     * Set fechavcto
     *
     * @param \DateTime $fechavcto
     * @return Transaccion
     */
    public function setFechavcto($fechavcto)
    {
        $this->fechavcto = $fechavcto;

        return $this;
    }

    /**
     * Get fechavcto
     *
     * @return \DateTime 
     */
    public function getFechavcto()
    {
        return $this->fechavcto;
    }

    /**
     * Set fechapago
     *
     * @param \DateTime $fechapago
     * @return Transaccion
     */
    public function setFechapago($fechapago)
    {
        $this->fechapago = $fechapago;

        return $this;
    }

    /**
     * Get fechapago
     *
     * @return \DateTime 
     */
    public function getFechapago()
    {
        return $this->fechapago;
    }

    /**
     * Set metodopago
     *
     * @param string $metodopago
     * @return Transaccion
     */
    public function setMetodopago($metodopago)
    {
        $this->metodopago = $metodopago;

        return $this;
    }

    /**
     * Get metodopago
     *
     * @return string 
     */
    public function getMetodopago()
    {
        return $this->metodopago;
    }

    /**
     * Set codigo_pago
     *
     * @param string $codigoPago
     * @return Transaccion
     */
    public function setCodigoPago($codigoPago)
    {
        $this->codigo_pago = $codigoPago;

        return $this;
    }

    /**
     * Get codigo_pago
     *
     * @return string 
     */
    public function getCodigoPago()
    {
        return $this->codigo_pago;
    }

    /**
     * Set estado_pago
     *
     * @param string $estadoPago
     * @return Transaccion
     */
    public function setEstadoPago($estadoPago)
    {
        $this->estado_pago = $estadoPago;

        return $this;
    }

    /**
     * Get estado_pago
     *
     * @return string 
     */
    public function getEstadoPago()
    {
        return $this->estado_pago;
    }

    /**
     * Set tipo_moneda
     *
     * @param string $tipoMoneda
     * @return Transaccion
     */
    public function setTipoMoneda($tipoMoneda)
    {
        $this->tipo_moneda = $tipoMoneda;

        return $this;
    }

    /**
     * Get tipo_moneda
     *
     * @return string 
     */
    public function getTipoMoneda()
    {
        return $this->tipo_moneda;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return Transaccion
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return string 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set certificado
     *
     * @param boolean $certificado
     * @return Transaccion
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;

        return $this;
    }

    /**
     * Get certificado
     *
     * @return boolean 
     */
    public function getCertificado()
    {
        return $this->certificado;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Transaccion
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->detalle = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add detalle
     *
     * @param \AppBundle\Entity\OrdenDetalle $detalle
     * @return Orden
     */
    public function addDetalle(\AppBundle\Entity\OrdenDetalle $detalle)
    {
        $this->detalle[] = $detalle;

        return $this;
    }

    /**
     * Remove detalle
     *
     * @param \AppBundle\Entity\OrdenDetalle $detalle
     */
    public function removeDetalle(\AppBundle\Entity\OrdenDetalle $detalle)
    {
        $this->detalle->removeElement($detalle);
    }

    /**
     * Get detalle
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }
}
