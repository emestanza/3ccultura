<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuario_perfil")
 */
class UsuarioPerfil
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=75)
     */
    private $nombre = '';

    /**
     * @var string $apellidopaterno
     *
     * @ORM\Column(name="apellidopaterno", type="string", length=75)
     */
    private $apellidopaterno;

    /**
     * @var string $apellidomaterno
     *
     * @ORM\Column(name="apellidomaterno", type="string", length=75, nullable=true)
     */
    private $apellidomaterno;

    /**
     * @var string $telefonomovil
     *
     * @ORM\Column(name="telefonomovil", type="string", length=20, nullable=true)
     */
    private $telefonomovil;

    /**
     * @var string $dni_ruc
     *
     * @ORM\Column(name="dni_ruc", type="string", length=20, nullable=true)
     */
    private $dni_ruc;

    /**
     * @var string $tipo_usuario
     *
     * @ORM\Column(name="tipo_usuario", type="string", length=20, nullable=true)
     */
    private $tipo_usuario;

    /**
     * @var string $fid_img
     *
     * @ORM\Column(name="fid_img", type="integer", nullable=true)
     */
    private $fid_img;

    /**
     * @var user
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", orphanRemoval=true)
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return UsuarioPerfil
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidopaterno
     *
     * @param string $apellidopaterno
     * @return UsuarioPerfil
     */
    public function setApellidopaterno($apellidopaterno)
    {
        $this->apellidopaterno = $apellidopaterno;

        return $this;
    }

    /**
     * Get apellidopaterno
     *
     * @return string 
     */
    public function getApellidopaterno()
    {
        return $this->apellidopaterno;
    }

    /**
     * Set apellidomaterno
     *
     * @param string $apellidomaterno
     * @return UsuarioPerfil
     */
    public function setApellidomaterno($apellidomaterno)
    {
        $this->apellidomaterno = $apellidomaterno;

        return $this;
    }

    /**
     * Get apellidomaterno
     *
     * @return string 
     */
    public function getApellidomaterno()
    {
        return $this->apellidomaterno;
    }

    /**
     * Set telefonomovil
     *
     * @param string $telefonomovil
     * @return UsuarioPerfil
     */
    public function setTelefonomovil($telefonomovil)
    {
        $this->telefonomovil = $telefonomovil;

        return $this;
    }

    /**
     * Get telefonomovil
     *
     * @return string 
     */
    public function getTelefonomovil()
    {
        return $this->telefonomovil;
    }

    /**
     * Set dni_ruc
     *
     * @param string $dniRuc
     * @return UsuarioPerfil
     */
    public function setDniRuc($dniRuc)
    {
        $this->dni_ruc = $dniRuc;

        return $this;
    }

    /**
     * Get dni_ruc
     *
     * @return string 
     */
    public function getDniRuc()
    {
        return $this->dni_ruc;
    }

    /**
     * Set tipo_usuario
     *
     * @param string $tipoUsuario
     * @return UsuarioPerfil
     */
    public function setTipoUsuario($tipoUsuario)
    {
        $this->tipo_usuario = $tipoUsuario;

        return $this;
    }

    /**
     * Get tipo_usuario
     *
     * @return string 
     */
    public function getTipoUsuario()
    {
        return $this->tipo_usuario;
    }

    /**
     * Set fid_img
     *
     * @param integer $fidImg
     * @return UsuarioPerfil
     */
    public function setFidImg($fidImg)
    {
        $this->fid_img = $fidImg;

        return $this;
    }

    /**
     * Get fid_img
     *
     * @return integer 
     */
    public function getFidImg()
    {
        return $this->fid_img;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return UsuarioPerfil
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
