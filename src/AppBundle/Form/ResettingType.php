<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class ResettingType extends AbstractType
{

     public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $builder
            ->add('email', 'email', array(
                'label'=>false,
                'attr' => array(
                    'placeholder' => 'Correo electrónico'
                )
            ))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $collectionConstraint = new Collection(array(
            'email' => array(
                new NotBlank(array('message' => 'El email no puede estar vacio.')),
                new Email(array('message' => 'Email con formato incorrecto.'))
            )
        ));

        $resolver->setDefaults(array(
            'constraints' => $collectionConstraint
        ));
    }

    public function getName()
    {
        return 'recuperar_usuario';
    }

}
