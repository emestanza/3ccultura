<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Persistence\ObjectManager;


class UserType extends AbstractType
{
    private $em;
    private $securityContext;

    public function __construct(SecurityContext $securityContext, ObjectManager $em)
    {
        $this->securityContext = $securityContext;
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('email', 'email', array(
                'label' => 'Correo electrónico',
                'required' => true
                ))
            ->add('plainPassword', 'password', array(
                'label'=> 'Crear contraseña',
                'required' => true
                ))
        ;


    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'validation_groups' => array('iniciar_sesion_usuario')
        ));
    }

    public function getName()
    {
        return 'usuario_creado';
    }
}
