<?php
/*
 * Conexión con Seguripago
 * =================================
 *
 * @author     Jhosbel Aguilar Rojas <jare.mac@gmail.com>
 * @version    2.0
 *
 */

namespace AppBundle\Util;

class SPApiEnvio
{

  /**
   * @var string API Id de seguripago usado para realizar 'requests'.
   */
  private $apiId;

  /**
   * @var string API key de seguripago usado para realizar 'requests'.
   */
  private $apiKey;

  /**
   * @var string API Modo de seguripago usado para realizar pruebas
   *             Cambiando de entorno:
   *             'test' -> Entorno de Testing de Seguripago
   *             'prod' -> Entorno de Producción de Seguripago
   */
  private $apiModo = 'test';

  /**
   * @var string|null La verisón del API usado para realizar 'requests'.
   */
  private $apiVersion = null;

  /**
   * @var string Número de pedido generado por el comercio.
   */
  private $numeroPedido;

  /**
   * @var timestamp Fecha y hora de venta.
   */
  private $fechaVenta;

  /**
   * @var timestamp Fecha y hora de vencimiento del Codigo de Seguripago.
   */
  private $fechaVencimiento;

  /**
   * @var string Moneda de venta (PEN/USD).
   */
  private $tipoMoneda;

  /**
   * @var float Importe total de la venta.
   */
  private $importeTotal;

  /**
   * @var array Importe total de la venta.
   */
  private $cliente = '';

  /**
   * @var array Importe total de la venta.
   */
  private $articulo = '';

  /**
   * @var string Corresponde a la interface que quiere que se muestre:
   *             Puede ser (H)orizontal o (V)ertical, si está en blanco o no existe,
   *             se considerará lo especificado en la configuración del Socio.
   */
  private $pantalla;

  /**
   * @var string Indica los medios de pago que serán excluidos
   *             para una determinada compra.
   */
  private $obviar;

  /**
   * @var string URL de Seguripago en modo producción, para envío de trama
   */
  const URLPROD = 'https://pagoin.seguripago.pe/pagoin.php';

  /**
   * @var string URL de Seguripago en modo producción, para confirmar recepción de data de pago
   */
  const URLPRODACUSE = 'https://pagoin.seguripago.pe/pagoin_acuse.php';

  /**
   * @var string URL de Seguripago en modo test (para pruebas), para envío de trama
   */
  const URLTEST = 'https://test.seguripago.pe/pagoin.php';

  /**
   * @var string URL de Seguripago en modo test (para pruebas), para confirmar recepción de data de pago
   */
  const URLTESTACUSE = 'https://test.seguripago.pe/pagoin_acuse.php';

  /**
   * @return string API Id de seguripago usado para los requests.
   */
  public function getApiId()
  {
    return $this->apiId;
  }
  /**
   * Sets API Id de seguripago usado para los requests.
   *
   * @param string $apiId
   */
  public function setApiId($apiId)
  {
    $this->apiId = $apiId;
  }

  /**
   * @return string API Key de seguripago usado para los requests.
   */
  public function getApiKey()
  {
    return $this->apiKey;
  }
  /**
   * Sets API Key de seguripago usado para los requests.
   *
   * @param string $apiKey
   */
  public function setApiKey($apiKey)
  {
    $this->apiKey = $apiKey;
  }

  /**
   * @return string Version del API usado para los requests. null por defecto.
   */
  public function getApiVersion()
  {
    return $this->apiVersion;
  }
  /**
   * @param string $apiVersion Version del API usado para los requests.
   */
  public function setApiVersion($apiVersion)
  {
    $this->apiVersion = $apiVersion;
  }

  /**
   * @return string Valor para poder realizar pruebas al entorno Testing o Producción.
   */
  public function getApiModo()
  {
    return $this->apiModo;
  }
  /**
   * @param string $apiModo Valor para poder realizar pruebas al entorno Testing o Producción.
   */
  public function setApiModo($apiModo)
  {
    $this->apiModo = $apiModo;
  }

  /**
   * @return string Número de pedido del comercio.
   */
  public function getNumeroPedido()
  {
    return $this->numeroPedido;
  }
  /**
   * @param string $numeroPedido Número de pedido del comercio.
   */
  public function setNumeroPedido($numeroPedido)
  {
    $this->numeroPedido = $numeroPedido;
  }

  /**
   * @return string Fecha de venta definido por el comercio.
   */
  public function getFechaVenta()
  {
    return $this->fechaVenta;
  }
  /**
   * @param string $fechaVenta Fecha de venta definido por el comercio.
   */
  public function setFechaVenta($fechaVenta)
  {
    $this->fechaVenta = $fechaVenta;
  }

  /**
   * @return string Fecha de Vencimiento del codigo de seguripago
   *                definido por el comercio.
   */
  public function getFechaVencimiento()
  {
    return $this->fechaVencimiento;
  }
  /**
   * @param string $fechaVencimiento Fecha de Vencimiento del codigo de seguripago
   *                                 definido por el comercio.
   */
  public function setFechaVencimiento($fechaVencimiento)
  {
    $this->fechaVencimiento = $fechaVencimiento;
  }

  /**
   * @return string Tipo de moneda definido por el comercio
   */
  public function getTipoMoneda()
  {
    return $this->tipoMoneda;
  }
  /**
   * @param string $tipoMoneda Tipo de moneda definido por el comercio
   */
  public function setTipoMoneda($tipoMoneda)
  {
    $this->tipoMoneda = $tipoMoneda;
  }

  /**
   * @return Datos del cliente concatenados con //
   */
  public function getCliente()
  {

    return $this->cliente;
  }
  /**
   * @param array $cliente Union en un string los valores del array cliente.
   * El string es generado con:
   * @implode('//', Array(código de cliente,nombre,apellido,razón social,
   *                      tipo documento,numero documento,email,dirección,país,sexo))
   */
  public function setCliente($cliente)
  {
    $str_cliente = '';

    if( !empty($cliente) )
    {
        $str_cliente = implode("//",$cliente);
    }

    $this->cliente = $str_cliente;
  }

  /**
   * @return Datos de los articulos concatenados con //
   */
  public function getArticulo()
  {

    return $this->articulo;
  }
  /**
   * @param array $articulo Union en un string los valores del array articulo.
   * El string es generado con:
   * @implode('//', Array(código de artículo,cantidad,precio))
   */
  public function setArticulo($articulo)
  {
    $str_articulo = '';

    if( !empty($articulo) )
    {
        $str_articulo = implode("//",$articulo);
    }

    $this->articulo = $str_articulo;
  }

  /**
   * @return float Importe total
   */
  public function getImporteTotal()
  {
    return $this->importeTotal;
  }
  /**
   * @param float $importeTotal Importe total
   */
  public function setImporteTotal($importeTotal)
  {
    $this->importeTotal = $importeTotal;
  }

  /**
   * @return string Corresponde a la interface que quiere que se muestre:
   * Puede ser (H)orizontal o (V)ertical
   */
  public function getPantalla()
  {
    return $this->pantalla;
  }
  /**
   * @param string $pantalla Corresponde a la interface que quiere que se muestre:
   * Puede ser (H)orizontal o (V)ertical
   */
  public function setPantalla($pantalla)
  {
    $this->pantalla = $pantalla;
  }

  /**
   * @return string Indica los medios de pago que serán excluidos de esta compra
   */
  public function getObviar()
  {
    return $this->obviar;
  }
  /**
   * @param string $obviar Indica los medios de pago que serán excluidos de esta compra
   */
  public function setObviar($obviar)
  {
    $this->obviar = $obviar;
  }

  /**
   * @return string URL de Seguripago para envío de trama
   */
  public function urlEnvioDatos()
  {

    if($this->apiModo == 'prod')
    {
        $url = SPApiEnvio::URLPROD;
    }
    else
    {
        $url = SPApiEnvio::URLTEST;
    }

    return $url;
  }

  /**
   * @return string URL de Seguripago para envío de acuse de recibo
   */
  public function urlAcuse()
  {

    if($this->apiModo == 'prod')
    {
        $url = SPApiEnvio::URLPRODACUSE;
    }
    else
    {
        $url = SPApiEnvio::URLTESTACUSE;
    }

    return $url;
  }

  /**
   * Generacion del HASH para verificar el envio de los datos
   *
   * @return string  Hash generado usando el algoritmo SHA1
   */
  public function generarHashEnvio()
  {

    $data=array();
    $data[] = $this->apiId;
    $data[] = $this->numeroPedido;
    $data[] = $this->fechaVenta;
    $data[] = $this->tipoMoneda;
    $data[] = $this->importeTotal;
    $data[] = $this->cliente;
    $data[] = $this->articulo;
    $data[] = $this->fechaVencimiento;
    $data[] = $this->apiKey;

    // Serializando toda la data a encriptar
    $cadena = implode("", $data);

    // Genrando Hash
    $hash = hash_hmac("sha1", $cadena, $this->apiKey);

    return $hash;

  }

  /**
   * Generacion del HASH para verificar la recepcion de los datos
   * Los siguientes parametros son enviados desde seguripago via POST
   *
   * @param  string $num_pedido     Numero de Pedido del Comercio
   * @param  string $cod_autoriza   Codigo de autorización
   * @param  string $num_referencia NUmero de referencia
   * @return string                 Hash generado usando el algoritmo SHA1
   */
  public function generarHashRecepcion($num_pedido, $cod_autoriza, $num_referencia)
  {

    $salt = "SEGURIPAGO";
    $data = array();
    $data[] = $this->apiId;
    $data[] = $this->apiKey;
    $data[] = $num_pedido;
    $data[] = $cod_autoriza;
    $data[] = $num_referencia;
    $data[] = $salt;

    // Serializando toda la data a encriptar
    $cadena = implode("", $data);

    $hash = hash_hmac("sha1", $cadena, $this->apiKey);

    // Genrando Hash
    return $hash;

  }

}