<?php

namespace AppBundle\Util;

/**
 * @note Valores de la tabla "Maestro"
 */
class Util
{

    public function limpiar_caracteres($s) {

        $s = preg_replace("[áàâãª]","a",$s);
        $s = preg_replace("[ÁÀÂÃ]","A",$s);
        $s = preg_replace("[éèê]","e",$s);
        $s = preg_replace("[ÉÈÊ]","E",$s);
        $s = preg_replace("[íìî]","i",$s);
        $s = preg_replace("[ÍÌÎ]","I",$s);
        $s = preg_replace("[óòôõº]","o",$s);
        $s = preg_replace("[ÓÒÔÕ]","O",$s);
        $s = preg_replace("[úùû]","u",$s);
        $s = preg_replace("[ÚÙÛ]","U",$s);
        $s = str_replace(" ","_",$s);
        $s = str_replace("ñ","n",$s);
        $s = str_replace("Ñ","N",$s);

        return $s;
    }

    const API_ID_SOCIO = '73';
    const API_KEY = 'bdb34a6d2368f4554b857833b4b43828';
    const API_MODO = 'prod';

}
