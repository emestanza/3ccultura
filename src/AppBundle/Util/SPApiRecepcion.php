<?php

namespace AppBundle\Util;

class SPApiRecepcion
{

    /**
     * @var string API Id de seguripago usado para validar el 'response'.
     */
    private $apiId;

    /**
     * @var string API key de seguripago usado para validar el 'response'.
     */
    private $apiKey;

    /**
     * @var string|null La verisón del API usado para validar el 'response'.
     */
    private $apiVersion = null;

    /**
     * @var int Request(01) Id del comercio o  socio SeguriPago
     */
    private $idSocio;

    /**
     * @var int Request(02) Numero de Pedido del comercio
     */
    private $numeroPedido;

    /**
     * @var int Request(03) Numero Unico de transacción de SeguriPago
     */
    private $numeroTransaccion;

    /**
     * @var timestamp Request(04) Fecha y hora de la transacción
     */
    private $fechaTransaccion;

    /**
     * @var string Request(05) Moneda de la transacción
     */
    private $moneda;

    /**
     * @var float Request(06) Importe de la transacción.
     */
    private $importeTransaccion;

    /**
     * @var integer Request(07) Resultado de la transacción.
     * 0: Generado,
     * 1: Aprobado,
     * 2: No aprobado,
     * *: Error.
     */
    private $resultadoTransaccion;

    /**
     * @var int Request(08) Código de acción generado por el banco.
     */
    private $codigoAccion;

    /**
     * @var string Request(09) Texto descriptivo del código de acción generado por el banco
     */
    private $descripcionCodigoAccion;

    /**
     * @var integer Request(010) Medio de pago utilizado:
     * 0: Otro
     * 1: Visa
     * 2: MasterCard
     * 3: American Express
     * *: Otro
     */
    private $medioPago;

    /**
     * @var int Request(011) Tipo de respuesta:
     * 1: Inmediato. La respuesta es inmediata
     * 2: Batch. La respuesta es diferida depende del cliente y lugar de pago
     */
    private $tipoRespuesta;

    /**
     * @var int Request(012) Código de autorización. Es el número asignado por la
     * entidad financiera.
     */
    private $codigoAutorizacion;

    /**
     * @var int Request(013) Número de referencia generado por el medio de pago.
     * Es el número asignado por la entidad financiera.
     */
    private $numeroReferencia;

    /**
     * @var string Request(014) HASH de la transacción
     */
    private $hashTransaccion;

    /**
     * @var integer Request(015) Código de Producto de SeguriPago:
     * 1: SeguriCrédito
     * 2: SeguriCash
     */
    private $codigoProductoSeguriCash;

    /**
     * @var integer Request(016) Número de tarjeta de crédito asteriscada
     */
    private $numeroTarjeta;

    /**
     * @var string Request(017) Nombre del tarjetahabiente en el caso de Visa
     */
    private $nombreTarjeta;

    /**
     * @var date Request(018) Fecha en la que finalizo la transacción
     */
    private $fechaFinTransaccion;


    /**
     * @return int
     */
    public function getIdSocio()
    {
        return $this->idSocio;
    }

    /**
     * @param int $idSocio
     */
    public function setIdSocio($idSocio)
    {
        $this->idSocio = $idSocio;
    }

    /**
     * @return int
     */
    public function getNumeroPedido()
    {
        return $this->numeroPedido;
    }

    /**
     * @param int $numeroPedido
     */
    public function setNumeroPedido($numeroPedido)
    {
        $this->numeroPedido = $numeroPedido;
    }

    /**
     * @return int
     */
    public function getNumeroTransaccion()
    {
        return $this->numeroTransaccion;
    }

    /**
     * @param int $numeroTransaccion
     */
    public function setNumeroTransaccion($numeroTransaccion)
    {
        $this->numeroTransaccion = $numeroTransaccion;
    }

    /**
     * @return timestamp
     */
    public function getFechaTransaccion()
    {
        return $this->fechaTransaccion;
    }

    /**
     * @param timestamp $fechaTransaccion
     */
    public function setFechaTransaccion($fechaTransaccion)
    {
        $this->fechaTransaccion = $fechaTransaccion;
    }

    /**
     * @return string
     */
    public function getMoneda()
    {
        return $this->moneda;
    }

    /**
     * @param string $moneda
     */
    public function setMoneda($moneda)
    {
        $this->moneda = $moneda;
    }

    /**
     * @return float
     */
    public function getImporteTransaccion()
    {
        return $this->importeTransaccion;
    }

    /**
     * @param float $importeTransaccion
     */
    public function setImporteTransaccion($importeTransaccion)
    {
        $this->importeTransaccion = $importeTransaccion;
    }

    /**
     * @return string
     */
    public function getResultadoTransaccion()
    {
        switch ($this->resultadoTransaccion) {

            case 0:
                $resultado =  "Generado";
                break;
            case 1:
                $resultado = "Aprobado";
                break;
            case 2:
                $resultado =  "No aprobado";
                break;
            default :
                $resultado =  "codigo inseperado";
                break;
        }

        return $resultado;
    }

    /**
     * @param int $resultadoTransaccion
     */
    public function setResultadoTransaccion($resultadoTransaccion)
    {
        $this->resultadoTransaccion = $resultadoTransaccion;
    }

    /**
     * @return int
     */
    public function getCodigoAccion()
    {
        return $this->codigoAccion;
    }

    /**
     * @param int $codigoAccion
     */
    public function setCodigoAccion($codigoAccion)
    {
        $this->codigoAccion = $codigoAccion;
    }

    /**
     * @return string
     */
    public function getDescripcionCodigoAccion()
    {
        return $this->descripcionCodigoAccion;
    }

    /**
     * @param string $descripcionCodigoAccion
     */
    public function setDescripcionCodigoAccion($descripcionCodigoAccion)
    {
        $this->descripcionCodigoAccion = $descripcionCodigoAccion;
    }

    /**
     * @return string
     */
    public function getMedioPago()
    {
        switch ($this->medioPago) {

            case 0 :
                $medio  = "Otros";
                break;
            case 1 :
                $medio  = "Visa";
                break;
            case 2 :
                $medio  = "MasterCard";
                break;
            case 3 :
                $medio  = "American Express";
                break;

        }

        return $medio;
    }

    /**
     * @param int $medioPago
     */
    public function setMedioPago($medioPago)
    {
        $this->medioPago = $medioPago;
    }

    /**
     * @return int
     */
    public function getTipoRespuesta()
    {

        switch ($this->tipoRespuesta) {

            case 1:
                $respuesta = "Inmediato";
                break;
            case 2:
                $respuesta = "Batch";

        }

        return $respuesta;
    }

    /**
     * @param int $tipoRespuesta
     */
    public function setTipoRespuesta($tipoRespuesta)
    {
        $this->tipoRespuesta = $tipoRespuesta;
    }

    /**
     * @return interger
     */
    public function getCodigoAutorizacion()
    {
        return $this->codigoAutorizacion;
    }

    /**
     * @param interger $codigoAutorizacion
     */
    public function setCodigoAutorizacion($codigoAutorizacion)
    {
        $this->codigoAutorizacion = $codigoAutorizacion;
    }

    /**
     * @return int
     */
    public function getNumeroReferencia()
    {
        return $this->numeroReferencia;
    }

    /**
     * @param int $numeroReferencia
     */
    public function setNumeroReferencia($numeroReferencia)
    {
        $this->numeroReferencia = $numeroReferencia;
    }

    /**
     * @return string
     */
    public function getHashTransaccion()
    {
        return $this->hashTransaccion;
    }

    /**
     * @param string $hashTransaccion
     */
    public function setHashTransaccion($hashTransaccion)
    {
        $this->hashTransaccion = $hashTransaccion;
    }

    /**
     * @return string
     */
    public function getCodigoProductoSeguriCash()
    {

        switch ($this->codigoProductoSeguriCash) {
            case 1:
                $prodSeguri = "SeguriCrédito";
                break;
            case 2:
                $prodSeguri = "SeguriCash";
                break;
        }

        return $prodSeguri;
    }

    /**
     * @param int $codigoProductoSeguriCash
     */
    public function setCodigoProductoSeguriCash($codigoProductoSeguriCash)
    {
        $this->codigoProductoSeguriCash = $codigoProductoSeguriCash;
    }

    /**
     * @return int
     */
    public function getNumeroTarjeta()
    {
        return $this->numeroTarjeta;
    }

    /**
     * @param int $numeroTarjeta
     */
    public function setNumeroTarjeta($numeroTarjeta)
    {
        $this->numeroTarjeta = $numeroTarjeta;
    }

    /**
     * @return string
     */
    public function getNombreTarjeta()
    {
        return $this->nombreTarjeta;
    }

    /**
     * @param string $nombreTarjeta
     */
    public function setNombreTarjeta($nombreTarjeta)
    {
        $this->nombreTarjeta = $nombreTarjeta;
    }

    /**
     * @return date
     */
    public function getFechaFinTransaccion()
    {
        return $this->fechaFinTransaccion;
    }

    /**
     * @param date $fechaFinTransaccion
     */
    public function setFechaFinTransaccion($fechaFinTransaccion)
    {
        $this->fechaFinTransaccion = $fechaFinTransaccion;
    }

    /**
     * @param array $request se envia todo el request de respuesta
     * para ser seteado en el objeto
     */
    public function setRespuestaRequest(array $request)
    {
        $this->setIdSocio($request["O1"]);
        $this->setNumeroPedido($request["O2"]);
        $this->setNumeroTransaccion($request["O3"]);
        $this->setFechaTransaccion($request["O4"]);
        $this->setMoneda($request["O5"]);
        $this->setImporteTransaccion($request["O6"]);
        $this->setResultadoTransaccion($request["O7"]);
        $this->setCodigoAccion($request["O8"]);
        $this->setDescripcionCodigoAccion($request["O9"]);
        $this->setMedioPago($request["O10"]);
        $this->setTipoRespuesta($request["O11"]);
        $this->setCodigoAutorizacion($request["O12"]);
        $this->setNumeroReferencia($request["O13"]);
        $this->setHashTransaccion($request["O14"]);
        $this->setCodigoProductoSeguriCash($request["O15"]);
        $this->setNumeroTarjeta($request["O16"]);
        $this->setNombreTarjeta($request["O17"]);
        $this->setFechaFinTransaccion($request["O18"]);
    }


}

