<?php

// Tested on PHP 5.2, 5.3

// This snippet (and some of the curl code) due to the Facebook SDK.
if (!function_exists('curl_init')) {
  throw new Exception('Seguripago needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
  throw new Exception('Seguripago needs the JSON PHP extension.');
}
if (!function_exists('mb_detect_encoding')) {
  throw new Exception('Seguripago needs the Multibyte String PHP extension.');
}

require(dirname(__FILE__) . '/Seguripago/Seguripago.php');
require(dirname(__FILE__) . '/Seguripago/Util.php');

