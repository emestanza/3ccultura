<?php

abstract class Seguripago_Util
{

  /**
   * Comprobar si el valor es un array o lista
   *
   * @param array|mixed $array
   * @return boolean True Si el objeto es una lista
   */
  public static function isList($array)
  {

    if (!is_array($array))
      return false;

    return true;

  }


}