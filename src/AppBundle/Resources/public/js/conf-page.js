/**
 * Created by enmanuel on 6/9/15.
 */
$(window).bind("load", function () {
    // code here
    var heights = $(".conf-cols").map(function () {
            return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);
    $(".conf-cols").height(maxHeight);
});

$(window).resize(function () {
    var heights = $(".conf-cols").map(function () {
            return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);
    $(".conf-cols").height(maxHeight);
});

window.$zopim || (function (d, s) {
    var z = $zopim = function (c) {
        z._.push(c)
    }, $ = z.s =
        d.createElement(s), e = d.getElementsByTagName(s)[0];
    z.set = function (o) {
        z.set.
            _.push(o)
    };
    z._ = [];
    z.set._ = [];
    $.async = !0;
    $.setAttribute("charset", "utf-8");
    $.src = "//v2.zopim.com/?30n734TN5Bou4t8I8Fer8j6rg93S4yyF";
    z.t = +new Date;
    $.
        type = "text/javascript";
    e.parentNode.insertBefore($, e)
})(document, "script");