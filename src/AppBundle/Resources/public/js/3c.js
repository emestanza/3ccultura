/**
 * Created by enmanuel on 4/15/15.
 */
function MainClass(e) {

}

MainClass.prototype = {constructor: function() {

}, getBaseUrl: function() {
    try {
        var e = location.href;
        var t = e.indexOf("//");
        if (t < 0)
            t = 0;
        else
            t = t + 2;
        var n = e.indexOf("/", t);
        if (n < 0)
            n = e.length - t;
        var r = e.substring(t, n);
        return"http://" + r
    } catch (i) {
        return null
    }
}, initSlider: function(e) {
   // $(e).bxSlider({pager: false, minSlides: 1, maxSlides: 4, slideWidth: 200})
}, initSlider2: function(e) {
    /*$(e).owlCarousel();
     var t = 0;
     var n = 0;
     $("img", "div.owl-item > div").each(function(e) {
     if ($(this).height() > t) {
     t = $(this).height()
     }
     if ($(this).width() > n) {
     n = $(this).width()
     }
     });
     $("div.owl-item > div").height(t).width(n)*/
}, initSelect2: function(e, t) {
    /*$(e).select2({placeholder: t, allowClear: true, formatNoMatches: function() {
     return"No hay opciones"
     }})*/
}, initpopOver: function(e, t, n) {
    /* $(e).popover({trigger: "hover", html: true, title: function() {
     return $(t).html()
     }, content: function() {
     return $(n).html()
     }, placement: "top"})*/
}, initCountdown: function(e) {
    /*var t = $(e);
     t.each(function(e) {
     var t = $(this).data("matchdate");
     var n = $(this).data("matchid");
     $(this).countdown({date: t, render: function(e) {
     if (e.days == 0 && e.hours == 0 && e.min == 0 && e.sec == 0) {
     $(this.el).html("");
     $(this.el).prev().html("CERRADAS LAS APUESTAS")
     } else
     $(this.el).html("<div>" + this.leadingZeros(e.days, 3) + " <span>días</span></div><div>" + this.leadingZeros(e.hours, 2) + " <span>hrs</span></div><div>" + this.leadingZeros(e.min, 2) + " <span>mins</span></div><div>" + this.leadingZeros(e.sec, 2) + " <span>segs</span></div>")
     }, onEnd: function() {
     var e = $(this.el);
     $.post(MainClass.prototype.getBaseUrl() + "/closematch", {matchId: n}).done(function(t) {
     e.html("");
     e.prev().html("CERRADAS LAS APUESTAS");
     e.parent().addClass("close-bets");
     e.parent().prev().find("input").hide();
     e.parent().parent().parent().next().hide()
     })
     }})
     })*/
},
    initModal: function(e) {
        $(e).modal("hide")
    },
    initModalShow: function(e) {
        $(e).modal("show");
    }
    , initRestrictOnlyNumbers: function(e) {
        $(e).keypress(function(e) {
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
                return false
        })
    }, initEvents: function(e, t) {
        var n = this;
        if ($.inArray("click", t) != -1) {
            $(document).on("click", e, function(t) {
                if (e == "#matches-tab") {
                    t.preventDefault();
                    $(".alert-warning").show()
                }

            })
        }
        if ($.inArray("change", t) != -1) {
            $(e).change(function(t) {
                if (e == ".champList") {
                    window.location = $(".champList option:selected").val()
                }

            })
        }
    },
    initPuzzleGrid: function(e, cols) {
        $(e).puzzleGrid({
            columnNumber: cols
        });
    }

}