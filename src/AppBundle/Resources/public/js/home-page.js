$(window).bind("load", function () {
    // code here
    var heights = $(".news-post").map(function () {
            return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);
    $(".news-post").height(maxHeight);
});

$(window).resize(function () {
    var heights = $(".news-post").map(function () {
            return $(this).height();
        }).get(),

        maxHeight = Math.max.apply(null, heights);
    $(".news-post").height(maxHeight);
});

$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    $('#defaultReal').realperson({
        regenerate: "Mostrar otra imagen",
    });


    $('.youtube').magnificPopup({
        type: 'iframe',
        iframe: {
            markup: '<div class="mfp-iframe-scaler">' +
            '<div class="mfp-close"></div>' +
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
            '</div>' // HTML markup of popup, `mfp-close` will be replaced by the close button
        }
    });


    $("#mc-embedded-subscribe-form").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                name: {
                    required: true
                },
                message: {
                    required: true
                }
                ,defaultReal: {
                    required: true
                }
            },
            errorClass: 'errorFormSubm',
            messages: {
                email:{
                  required: "Ingrese un email",
                  email: "Ingrese un email válido"
                } ,
                name: "Ingrese un Nombre",
                message: "Ingrese una consulta",
                defaultReal: "Ingrese el texto captcha"


            }
        }
    );

    $('.cd-testimonials-wrapper').flexslider({
        //declare the slider items
        selector: ".cd-testimonials > li",
        animation: "slide",
        slideshowSpeed: 10000,
        //do not add navigation for paging control of each slide
        controlNav: false,
        directionNav: false,
        slideshow: true,
        //Allow height of the slider to animate smoothly in horizontal mode
      /*  smoothHeight: true,*/
        start: function(){
            $('.cd-testimonials').children('li').css({
                'opacity': 1
                /*'position': 'relative'*/
            });
        }
    });

    //resizing videos from last news
    $("iframe").width("100%");
    $("iframe").removeAttr("height");

});