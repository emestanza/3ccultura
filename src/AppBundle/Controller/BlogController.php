<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BlogController extends Controller {

    public function indexAction(Request $request) {

        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $dql   = "SELECT a FROM KayueWordpressBundle:Post a WHERE a.type='post' and a.status='publish' ORDER BY a.date DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page numpber*/,
            5/*limit per page*/
        );

       return $this->render("AppBundle:Blog:index.html.twig", array(
           "posts" => $posts,
           "section" => "blog",
           'wp_url' => $this->container->getParameter('wp_url'),
       ));

    }

    public function getAction(Request $request, $id) {

        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $repo = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager')->getRepository('KayueWordpressBundle:Post');
        $post = $repo->findOneBySlug($id);

        if (!$post) {
            throw $this->createNotFoundException(
                'Post no encontrado '
            );
        }
        $date = $post->getDate();
        $formatter = new \IntlDateFormatter("es_ES", \IntlDateFormatter::LONG, \IntlDateFormatter::LONG);
        $formatter->setPattern('d MMMM Y');
        $date = ucwords($formatter->format($date));

        return $this->render("AppBundle:Blog:detail.html.twig", array(
            "post" => $post,
            "postDate" => $date,
            "section" => "blog",
            'wp_url' => $this->container->getParameter('wp_url'),
        ));

    }

}