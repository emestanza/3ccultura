<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\RedirectResponse;

use AppBundle\Entity\User;
use AppBundle\Entity\UsuarioPerfil;
use AppBundle\Entity\Orden;
use AppBundle\Entity\OrdenDetalle;

use AppBundle\Util\SPApiEnvio;
use AppBundle\Util\SPApiRecepcion;
use AppBundle\Util\Util;

class PaymentController extends Controller
{

    private $api = "prod";

    public function payment_resultadoAction(Request $request)
    {

        /*if (strpos($request->getHttpHost(), "testing") >= 0) {
            $this->api = "test";
        }*/

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        $mensaje = '';

        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $redirect_url = $this->generateUrl('index');
            return new RedirectResponse($redirect_url);
        }

        /**
         * Información sobre data recibida:
         *
         * $_POST['O1']  => "idSocio"            : Identificador de Socio
         * $_POST['O2']  => "num_pedido"         : Número de pedido de Socio
         * $_POST['O3']  => "num_transaccion"    : Número de transacción generado por Seguripago
         * $_POST['O4']  => "fecha_hora_trans"   : Fecha/hora de transacción en Unixtime
         * $_POST['O5']  => "moneda"             : Moneda
         * $_POST['O6']  => "importe"            : Importe aprobado
         * $_POST['O7']  => "resultado"          : Resultado de la transaccion. Generado (0), Aprobado (1), No aprobado (2)
         * $_POST['O8']  => "cod_respuesta"      : Código de respuesta, generado por el medio de pago
         * $_POST['O9']  => "txt_respuesta"      : Texto descriptivo de respuestas, generado por el medio de pago
         * $_POST['O10'] => "medio_pago"        : Código de Medio de pago utilizado para SeguriCrédito (si es Seguricash se envía cero (0)). (1) Visa, (2) Mastercard, (3) American Express
         * $_POST['O11'] => "tipo_respuesta"    : Tipo de respuestas: Inmediato (1), Batch (2)
         * $_POST['O12'] => "cod_autoriza"      : Código de autorización, enviado por algunos medios de pago
         * $_POST['O13'] => "num_referencia"    : Número de referencia, enviado por algunos medios e pago
         * $_POST['O14'] => "hash"              : HASH de validacion
         * $_POST['O15'] => "cod_producto"      : Código del Producto de SeguriPago: (1) SeguriCrédito, (2) SeguriCash.
         * $_POST['O16'] => "num_tarjeta"       : Numero de tarjeta asteriscada.
         * $_POST['O17'] => "nom_tarjetahabiente": nombre del titular de  la tarjeta, solo algunos medios de pago.
         * $_POST['O18'] => "fecha de vencimiento": Usado para las transacciones con seguricash
         *
         */

        $idSocio = $request->request->get('O1');
        $num_pedido = $request->request->get('O2');
        $num_transaccion = $request->request->get('O3');
        $fecha_hora_trans = $request->request->get('O4');
        $moneda = $request->request->get('O5');
        $importe = $request->request->get('O6');
        $resultado = $request->request->get('O7');
        $cod_respuesta = $request->request->get('O8');
        $txt_respuesta = $request->request->get('O9');
        $medio_pago = $request->request->get('O10');
        $tipo_respuesta = $request->request->get('O11');
        $cod_autoriza = $request->request->get('O12');
        $num_referencia = $request->request->get('O13');
        $hash = $request->request->get('O14');
        $cod_producto = $request->request->get('O15');
        $num_tarjeta = $request->request->get('O16');
        $nom_tarjetahabiente = $request->request->get('O17');
        $fecha_vencimiento = $request->request->get('O18');

        $id_u_curso = $num_pedido * 1;
        $orden = $em->getRepository('AppBundle:Orden')->find($id_u_curso);

        if (!$orden) {
            $url = $this->generateUrl('index');

            return new RedirectResponse($url);
        }

        $user = $securityContext->getToken()->getUser();

        $user_perfil = new UsuarioPerfil();

        $user_perfil = $em->getRepository('AppBundle:UsuarioPerfil')->findOneByUser($user);

        $nombre_usuario = '';
        if ($user_perfil) {

            // $nombre_usuario = !empty($user_perfil->getNombre())? $user_perfil->getNombre(): '';
            // $nombre_usuario .= " ".!empty($user_perfil->getApellidopaterno())? $user_perfil->getApellidopaterno(): '';
            // $nombre_usuario .= " ".!empty($user_perfil->getApellidomaterno())? $user_perfil->getApellidomaterno(): '';
        }

        $sp_api = new SPApiEnvio();

        $sp_api->setApiId(Util::API_ID_SOCIO);
        $sp_api->setApiKey(Util::API_KEY);
        $sp_api->setApiModo($this->api);

        $medio_de_pago = $medio_pago == "1" ? "Visa" : ($medio_pago == "2" ? "Mastercard" : ($medio_pago == "3" ? "American Express" : "Otro"));

        $moneda = ($moneda == "PEN") ? "S/." : $moneda;
        $fecha_transaccion = date("d/m/Y", $fecha_hora_trans) . " a las " . date("H:i", $fecha_hora_trans);

        $hashValidate = $sp_api->generarHashRecepcion($num_pedido, $cod_autoriza, $num_referencia);
        $hash_acuse = hash_hmac("sha1", $idSocio . $num_transaccion, Util::API_KEY);

        $url_acuse = $sp_api->urlAcuse();

        if ($hash == $hashValidate) {
            if ($cod_producto == "1") {
                if ($resultado == "1") {
                    $estado_pago = 'Pagado';
                } else {
                    $estado_pago = 'Denegado';
                }

            } elseif ($cod_producto == "2") {
                if ($resultado == "0") {
                    $estado_pago = 'Pendiente';

                } elseif ($resultado == "1") {
                    $estado_pago = 'Pagado';
                }

            } elseif ($cod_producto == "6") {
                if ($resultado == "1") {
                    $estado_pago = 'Pagado';
                } else {
                    $estado_pago = 'Denegado';
                }

            }

            $fecha_trans = new \DateTime();
            $fecha_trans->setTimestamp($fecha_hora_trans);

            $orden->setFechapago($fecha_trans);
            $orden->setEstadoPago($estado_pago);
            $orden->setCodigoPago($num_transaccion);

            $conferencia = $em->getRepository('AppBundle:Conferencia')->find(1);
            $orden_detalle = new OrdenDetalle();
            $orden_detalle->setConferencia($conferencia);
            $orden_detalle->setCantidad('1');
            $orden_detalle->setPrecio($importe);
            $orden_detalle->setOrden($orden);


            $sessionObj = $this->get('session');
            //$sessionObj->set("exteriorPais", $request->getQueryString("exteriorPais"));

            /* $orden_detalle->setExteriorPais($sessionObj->get("exteriorPais"));
             if($sessionObj->get("exteriorPais") != "PE"){
                /* $sessionObj->set("exteriorNombre", $request->getQueryString("exteriorNombre"));
                 $sessionObj->set("exteriorDirecc", $request->getQueryString("exteriorDirecc"));*/
            /*  $orden_detalle->setExteriorDireccion($sessionObj->get('exteriorDirecc'));
              $orden_detalle->setExteriorEmpresa($sessionObj->get('exteriorNombre'));*/
            /*  }
              else{
                  if ($sessionObj->get("boletaNombreCompleto") != ""){
                     /* $sessionObj->set("boletaNombreCompleto", $request->getQueryString("boletaNombreCompleto"));
                      $sessionObj->set("boletaDni", $request->getQueryString("boletaDni"));*/
            /*         $orden_detalle->setBoletaDni($sessionObj->get('boletaDni'));
                     $orden_detalle->setBoletaNombre($sessionObj->get('boletaNombreCompleto'));
                 }
                 else{
                    /* $sessionObj->set("facturaRazonSocial", $request->getQueryString("facturaRazonSocial"));
                     $sessionObj->set("facturaRUC", $request->getQueryString("facturaRUC"));
                     $sessionObj->set("facturaDirecc", $request->getQueryString("facturaDirecc"));*/
            /*         $orden_detalle->setFacturaDireccion($sessionObj->get('facturaDirecc'));
                     $orden_detalle->setFacturaRazonSocial($sessionObj->get('facturaRazonSocial'));
                     $orden_detalle->setFacturaRuc($sessionObj->get('facturaRUC'));
                 }
             }*/

            /*$paymentOption = $request->request->get('paymentOption');

            if ($paymentOption == "boleta") {
                $orden_detalle->setBoletaDni($request->request->get('boletaDni'));
                $orden_detalle->setBoletaNombre($request->request->get('boletaNombreCompleto'));
            }

            if ($paymentOption == "factura") {
                $orden_detalle->setFacturaDireccion($request->request->get('facturaDirecc'));
                $orden_detalle->setFacturaRazonSocial($request->request->get('facturaRazonSocial'));
                $orden_detalle->setFacturaRuc($request->request->get('facturaRUC'));
            }

            if ($paymentOption == "exterior") {
                $orden_detalle->setExteriorDireccion($request->request->get('exteriorDirecc'));
                $orden_detalle->setExteriorEmpresa($request->request->get('exteriorNombre'));
                $orden_detalle->setExteriorPais($request->request->get('exteriorPais'));

            }*/

            $em = $this->getDoctrine()->getManager();
            $em->persist($orden);
            $em->persist($orden_detalle);
            $em->flush();

            if ($estado_pago == "Pagado") {

                $documento = $this->container->getParameter('kernel.root_dir') . '/../web/assets/recomendaciones.pdf';
                $documento2 = $this->container->getParameter('kernel.root_dir') . '/../web/assets/material_para_participantes.pdf';

                $message = \Swift_Message::newInstance()
                    ->setSubject('Confirmación de Pago')
                    ->setFrom('ayuda@3ccultura.com', "3c Cultura")
                    ->setTo($this->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                            'AppBundle:Payment:email.txt.twig', array("baseUrl"=> $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath(), "orden" => $orden)
                        ), 'text/html'
                    )
                    ->attach(\Swift_Attachment::fromPath($documento))
                    ->attach(\Swift_Attachment::fromPath($documento2));

                $this->get('mailer')->send($message);

            }
        } else {
            $mensaje = "Error al recepcionar datos. Contáctese con el administrador";

        }

        return $this->render("AppBundle:Payment:payment_resultado.html.twig", array(
            'idsocio' => $idSocio,
            'num_transaccion' => $num_transaccion,
            'num_pedido' => $num_pedido,
            'importe' => $importe,
            'cod_producto' => $cod_producto,
            'resultado' => $resultado,
            'id_u_curso' => '$id_u_curso',
            'txt_respuesta' => $txt_respuesta,
            'cod_autoriza' => $cod_autoriza,
            'num_referencia' => $num_referencia,
            'num_tarjeta' => $num_tarjeta,
            'nom_tarjetahabiente' => $nom_tarjetahabiente,
            'fecha_vencimiento' => $fecha_vencimiento,
            'mensaje' => $mensaje,
            'medio_de_pago' => $medio_de_pago,
            'moneda' => $moneda,
            'fecha_transaccion' => $fecha_transaccion,
            'nombre_usuario' => $nombre_usuario,
            'hash_acuse' => $hash_acuse,
            'url_acuse' => $url_acuse
        ));

    }


    public function payment_notificacionAction(Request $request)
    {

        /*if (strpos($request->getHttpHost(), "testing") >= 0) {
            $this->api = "test";
        }*/

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        $mensaje = '';

        /**
         * Información sobre data recibida:
         *
         * $_POST['O1']  => "idSocio"            : Identificador de Socio
         * $_POST['O2']  => "num_pedido"         : Número de pedido de Socio
         * $_POST['O3']  => "num_transaccion"    : Número de transacción generado por Seguripago
         * $_POST['O4']  => "fecha_hora_trans"   : Fecha/hora de transacción en Unixtime
         * $_POST['O5']  => "moneda"             : Moneda
         * $_POST['O6']  => "importe"            : Importe aprobado
         * $_POST['O7']  => "resultado"          : Resultado de la transaccion. Generado (0), Aprobado (1), No aprobado (2)
         * $_POST['O8']  => "cod_respuesta"      : Código de respuesta, generado por el medio de pago
         * $_POST['O9']  => "txt_respuesta"      : Texto descriptivo de respuestas, generado por el medio de pago
         * $_POST['O10'] => "medio_pago"        : Código de Medio de pago utilizado para SeguriCrédito (si es Seguricash se envía cero (0)). (1) Visa, (2) Mastercard, (3) American Express
         * $_POST['O11'] => "tipo_respuesta"    : Tipo de respuestas: Inmediato (1), Batch (2)
         * $_POST['O12'] => "cod_autoriza"      : Código de autorización, enviado por algunos medios de pago
         * $_POST['O13'] => "num_referencia"    : Número de referencia, enviado por algunos medios e pago
         * $_POST['O14'] => "hash"              : HASH de validacion
         * $_POST['O15'] => "cod_producto"      : Código del Producto de SeguriPago: (1) SeguriCrédito, (2) SeguriCash.
         * $_POST['O16'] => "num_tarjeta"       : Numero de tarjeta asteriscada.
         * $_POST['O17'] => "nom_tarjetahabiente": nombre del titular de  la tarjeta, solo algunos medios de pago.
         * $_POST['O18'] => "fecha de vencimiento": Usado para las transacciones con seguricash
         *
         */

        $idSocio = $request->request->get('O1');
        $num_pedido = $request->request->get('O2');
        $num_transaccion = $request->request->get('O3');
        $fecha_hora_trans = $request->request->get('O4');
        $moneda = $request->request->get('O5');
        $importe = $request->request->get('O6');
        $resultado = $request->request->get('O7');
        $cod_respuesta = $request->request->get('O8');
        $txt_respuesta = $request->request->get('O9');
        $medio_pago = $request->request->get('O10');
        $tipo_respuesta = $request->request->get('O11');
        $cod_autoriza = $request->request->get('O12');
        $num_referencia = $request->request->get('O13');
        $hash = $request->request->get('O14');
        $cod_producto = $request->request->get('O15');
        $num_tarjeta = $request->request->get('O16');
        $nom_tarjetahabiente = $request->request->get('O17');
        $fecha_vencimiento = $request->request->get('O18');

        $id_u_curso = $num_pedido * 1;
        $orden = $em->getRepository('AppBundle:Orden')->find($id_u_curso);

        if (!$orden) {

            $mensaje = "No existe registro";

            print $mensaje;
            exit();

        }

        $sp_api = new SPApiEnvio();

        $sp_api->setApiId(Util::API_ID_SOCIO);
        $sp_api->setApiKey(Util::API_KEY);
        $sp_api->setApiModo($this->api);

        $medio_de_pago = $medio_pago == "1" ? "Visa" : ($medio_pago == "2" ? "Mastercard" : ($medio_pago == "3" ? "American Express" : ($medio_pago == "13" ? "Visa - Débito" : "Otro")));

        $moneda = ($moneda == "PEN") ? "S/." : $moneda;
        $fecha_transaccion = date("d/m/Y", $fecha_hora_trans) . " a las " . date("H:i", $fecha_hora_trans);

        $hashValidate = $sp_api->generarHashRecepcion($num_pedido, $cod_autoriza, $num_referencia);
        $hash_acuse = hash_hmac("sha1", $idSocio . $num_transaccion, Util::API_KEY);
        $url_acuse = $sp_api->urlAcuse();

        if ($hash == $hashValidate) {
            if ($cod_producto == "1") {
                if ($resultado == "1") {

                    $estado_pago = 'Pagado';
                } else {
                    $estado_pago = 'Denegado';
                }

            } elseif ($cod_producto == "2") {

                if ($resultado == "0") {

                    $estado_pago = 'Pendiente';

                } elseif ($resultado == "1") {

                    $estado_pago = 'Pagado';
                }

            } elseif ($cod_producto == "6") {
                if ($resultado == "1") {
                    $estado_pago = 'Pagado';
                } else {
                    $estado_pago = 'Denegado';
                }

            }

            $fecha_trans = new \DateTime();
            $fecha_trans->setTimestamp($fecha_hora_trans);

            $orden->setFechapago($fecha_trans);
            $orden->setEstadoPago($estado_pago);
            $orden->setCodigoPago($num_transaccion);

            $em = $this->getDoctrine()->getManager();
            $em->persist($orden);
            $em->flush();

        } else {

            $mensaje = "Error al recepcionar datos. Contáctese con el administrador";

            print $mensaje;
            exit();

        }

        if ($this->api == "test") {
            return $this->forward('AppBundle:Payment:payment_resultado', array('request' => $request));
        }

        return $this->render("AppBundle:Payment:payment_notificacion.html.twig", array(
            'idsocio' => $idSocio,
            'num_transaccion' => $num_transaccion,
            'importe' => $importe,
            'cod_producto' => $cod_producto,
            'resultado' => $resultado,
            'id_u_curso' => $id_u_curso,
            'txt_respuesta' => $txt_respuesta,
            'cod_autoriza' => $cod_autoriza,
            'num_referencia' => $num_referencia,
            'num_tarjeta' => $num_tarjeta,
            'nom_tarjetahabiente' => $nom_tarjetahabiente,
            'fecha_vencimiento' => $fecha_vencimiento,
            'mensaje' => $mensaje,
            'medio_de_pago' => $medio_de_pago,
            'moneda' => $moneda,
            'fecha_transaccion' => $fecha_transaccion,
            'hash_acuse' => $hash_acuse,
            'url_acuse' => $url_acuse
        ));

    }


    public function payment_envioAction(Request $request)
    {

        $form = $this->getFormSendData();
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $securityContext = $this->container->get('security.context');
            $user = $securityContext->getToken()->getUser();

            if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

                $url = $this->generateUrl('index');
                return new RedirectResponse($url);
            }

            // Sesion 
            $session = $this->getRequest()->getSession();
            //$cart = $cart = $session->get('cart', array());

            $conferencia = $em->getRepository('AppBundle:Conferencia')->find(1);

            if (!$conferencia) {
                $url = $this->generateUrl('index');
                return new RedirectResponse($url);
            }

            $fecha_venta = new \DateTime('now');
            $fecha_vecto = new \DateTime('+2 days');
            $metodo_pago = 'Seguripago';
            $estado_pago = 'Pendiente';
            $tipo_moneda = 'PEN';
            $importe_curso = 89;

            $orden = new Orden();
            $orden->setUser($user);
            $orden->setFecharegistro($fecha_venta);
            $orden->setFechavcto($fecha_vecto);
            $orden->setMetodoPago($metodo_pago);
            $orden->setEstadoPago($estado_pago);
            $orden->setTipoMoneda($tipo_moneda);
            $orden->setImporte($importe_curso);

            $orden_detalle = new OrdenDetalle();
            $orden_detalle->setConferencia($conferencia);
            $orden_detalle->setCantidad('1');
            $orden_detalle->setPrecio($importe_curso);
            $orden_detalle->setOrden($orden);

            /*$paymentOption = $request->request->get('paymentOption');

            if ($paymentOption == "boleta") {
                $orden_detalle->setBoletaDni($request->request->get('boletaDni'));
                $orden_detalle->setBoletaNombre($request->request->get('boletaNombreCompleto'));
            }

            if ($paymentOption == "factura") {
                $orden_detalle->setFacturaDireccion($request->request->get('facturaDirecc'));
                $orden_detalle->setFacturaRazonSocial($request->request->get('facturaRazonSocial'));
                $orden_detalle->setFacturaRuc($request->request->get('facturaRUC'));
            }

            if ($paymentOption == "exterior") {
                $orden_detalle->setExteriorDireccion($request->request->get('exteriorDirecc'));
                $orden_detalle->setExteriorEmpresa($request->request->get('exteriorNombre'));
                $orden_detalle->setExteriorPais($request->request->get('exteriorPais'));

            }*/

            $em = $this->getDoctrine()->getManager();

            $em->persist($orden);
            $em->persist($orden_detalle);
            $em->flush();

            $num_pedido = str_pad($orden->getId(), 5, "0", STR_PAD_LEFT);

            $sp_api = new SPApiEnvio();

            $sp_api->setApiId(Util::API_ID_SOCIO);
            $sp_api->setApiKey(Util::API_KEY);
            $sp_api->setApiModo($this->api);
            $sp_api->setNumeroPedido($num_pedido);
            $sp_api->setFechaVenta($fecha_venta->getTimestamp());
            $sp_api->setFechaVencimiento($fecha_vecto->getTimestamp());
            $sp_api->setTipoMoneda($tipo_moneda);
            $sp_api->setImporteTotal($importe_curso);

            return $this->render("AppBundle:Payment:payment_envio.html.twig", array(
                'sp_api' => $sp_api
            ));
        }

        $url = $this->generateUrl('index');

        return new RedirectResponse($url);

    }

    private function getFormSendData()
    {
        return $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('payment_envio'))
            ->add('idcurso', 'hidden')
            ->add('importe', 'hidden')
            ->add('tipo_moneda', 'hidden')
            ->add('nombre_usuario', 'hidden')
            ->add('save', 'submit', array('label' => 'PAGAR'))
            ->getForm();
    }

    public function payment_historialAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');

        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $redirect_url = $this->generateUrl('index');
            return new RedirectResponse($redirect_url);
        }

        $user = $securityContext->getToken()->getUser();

        $cursos = $em->getRepository('AppBundle:Orden')->findByUser($user);

        return $this->render("AppBundle:Payment:payment_historial.html.twig", array(
            'cursos' => $cursos
        ));
    }


    public function detallePagoAction(Request $request)
    {

        if ($request->getMethod() == 'POST') {

            $em = $this->getDoctrine()->getManager();
            $securityContext = $this->container->get('security.context');
            //$user = $securityContext->getToken()->getUser();

            if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

                $url = $this->generateUrl('index');
                return new RedirectResponse($url);
            }

            $conferencia = $em->getRepository('AppBundle:Conferencia')->find(1);

            if (!$conferencia) {
                $url = $this->generateUrl('index');
                return new RedirectResponse($url);
            }

            $orden = $em->getRepository('AppBundle:Orden')->find($request->request->get('idOrden'));

            $orden_detalle = new OrdenDetalle();
            $orden_detalle->setConferencia($conferencia);
            $orden_detalle->setCantidad('1');
            $orden_detalle->setPrecio($orden->getImporte());
            $orden_detalle->setOrden($orden);

            if ($request->request->get('boletaDni') != "") {
                $orden_detalle->setBoletaDni($request->request->get('boletaDni'));
                $orden_detalle->setBoletaNombre($request->request->get('boletaNombreCompleto'));
            }

            if ($request->request->get('facturaDirecc') != "") {
                $orden_detalle->setFacturaDireccion($request->request->get('facturaDirecc'));
                $orden_detalle->setFacturaRazonSocial($request->request->get('facturaRazonSocial'));
                $orden_detalle->setFacturaRuc($request->request->get('facturaRUC'));
            }

            if ($request->request->get('exteriorDirecc') != "") {
                $orden_detalle->setExteriorDireccion($request->request->get('exteriorDirecc'));
                $orden_detalle->setExteriorEmpresa($request->request->get('exteriorNombre'));
                $orden_detalle->setExteriorPais($request->request->get('exteriorPais'));

            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($orden_detalle);
            $em->flush();

            return $this->render("AppBundle:Payment:detalle_pago.html.twig", array("mensaje" => "La información fue guardada exitosamente"));

        }

        return $this->render("AppBundle:Payment:detalle_pago.html.twig", array("idOrden" => $request->query->get('o')));
    }
}
