<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\UsuarioPerfil;
use AppBundle\Form\UsuarioPerfilType;
use Kayue\WordpressBundle\Entity\Post;


class ConferenciaController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user_register = new UsuarioPerfil();
        $form = $this->createFormRegister($user_register);
        $form->handleRequest($request);
        $conferencias = $em->getRepository('AppBundle:Conferencia')->findAll();

        return $this->render("AppBundle:Conferencia:conferencia_virtual.html.twig", array(
            'form' => $form->createView(),
            'conferencia' => $conferencias,
            'registroSuccess' => $request->query->get("registroSuccess")
        ));
    }

    public function detalleAction(Request $request, $uri)
    {
        $em = $this->getDoctrine()->getManager();
        $conferencia = $em->getRepository('AppBundle:Conferencia')->findOneByUri($uri);
        $id = $conferencia->getId();

        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $paginator  = $this->get('knp_paginator');

        $relatedQ =  $em->createQueryBuilder()
            ->from('KayueWordpressBundle:Post', 'post')
            ->select('post')
            ->leftJoin('post.taxonomies', 'tax')
            ->leftJoin('tax.term', 'term')
            ->andWhere('tax.name = :tax')->andWhere('term.slug = :term')
            ->andWhere('post.status = \'publish\'')
            ->setParameter('tax', 'category')
            ->setParameter('term', 'gestion-del-tiempo')
            ->getQuery();

        $related = $paginator->paginate(
            $relatedQ,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        return $this->render("AppBundle:Conferencia:detalle.html.twig", array(
            'conferencia' => $conferencia,
            "relatedPosts" => $related,
            'wp_url' => $this->container->getParameter('wp_url'),
        ));
    }


    public function updateEmailConference()
    {
        $em = $this->getDoctrine()->getManager();

        $dateTime = new \DateTime("now");

        echo($dateTime);
        die();

        //$qb = $em->createQueryBuilder();
        //$qb->from('AppBundle:HorarioConferencia', 'h');

       // return $qb;
    }


    /**
     * Crear formulario para registrar un nuevo usuario
     *
     * @param User $entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFormRegister(UsuarioPerfil $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new UsuarioPerfilType($securityContext, $em), $usuario, array(
            'method' => 'POST',
            'action' => 'registro-usuario'
        ));

        $form->add('save', 'submit', array('label' => 'Deseo recibir encuesta gratuita', 'attr' => array('class' => 'btn-default')));

        return $form;
    }



}