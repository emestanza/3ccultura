<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\UsuarioPerfil;

class SiteController extends Controller {

    public function indexAction(Request $request) {

        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $dql   = "SELECT a FROM KayueWordpressBundle:Post a WHERE a.type='post' and a.status='publish' ORDER BY a.date DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        $em = $this->getDoctrine()->getManager();
        $testimonios= $em->getRepository('AppBundle:Testimonio')->findAll();

        return $this->render("AppBundle:Site:index.html.twig", array(
            "posts" => $posts,
            "testimonios" => $testimonios,
            'wp_url' => $this->container->getParameter('wp_url'),
            "message" => $request->query->get('m')
        ));
    }

    public function aboutAction(Request $request) {
        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $dql   = "SELECT a FROM KayueWordpressBundle:Post a WHERE a.type='post' and a.status='publish' ORDER BY a.date DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        $relatedQ =  $em->createQueryBuilder()
            ->from('KayueWordpressBundle:post', 'post')
            ->select('post')
            ->leftJoin('post.taxonomies', 'tax')
            ->leftJoin('tax.term', 'term')
            ->andWhere('tax.name = :tax')->andWhere('term.slug = :term')
            ->andWhere('post.status = \'publish\'')
            ->setParameter('tax', 'category')
            ->setParameter('term', 'management')
            ->getQuery();

        $related = $paginator->paginate(
            $relatedQ,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        return $this->render("AppBundle:Site:about.html.twig", array(
            "posts" => $posts,
            "relatedPosts" => $related,
            'wp_url' => $this->container->getParameter('wp_url'),
        ));
    }

    public function consultantAction(Request $request) {

        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $dql   = "SELECT a FROM KayueWordpressBundle:Post a WHERE a.type='post' and a.status='publish' ORDER BY a.date DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        $relatedQ =  $em->createQueryBuilder()
            ->from('KayueWordpressBundle:post', 'post')
            ->select('post')
            ->leftJoin('post.taxonomies', 'tax')
            ->leftJoin('tax.term', 'term')
            ->andWhere('tax.name = :tax')->andWhere('term.slug = :term')
            ->andWhere('post.status = \'publish\'')
            ->setParameter('tax', 'category')
            ->setParameter('term', 'recursos-humanos')
            ->getQuery();

        $related = $paginator->paginate(
            $relatedQ,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        return $this->render("AppBundle:Site:consultant.html.twig", array(
            "posts" => $posts,
            "relatedPosts" => $related,
            'wp_url' => $this->container->getParameter('wp_url'),
        ));
    }

    public function libro_reclamacionAction(Request $request)
    {

        $mensaje = "";
        if ($request->getMethod() == 'POST') {

            $message = \Swift_Message::newInstance()
                ->setSubject('Reclamo')
                ->setFrom($request->request->get('reclaEmail'), $request->request->get('reclaNombreApellido'))
                ->setTo("reclamos@3ccultura.com")
                //->setTo("emijacobo@gmail.com")
                ->setBody(
                    $this->renderView(
                        'AppBundle:Site:email.html.twig', array(
                            "nombreApellido" => $request->request->get('reclaNombreApellido'),
                            "docIdent" => $request->request->get('reclaDocIdentidad'),
                            "direccion" => $request->request->get('reclaDireccion'),
                            "nroDoc" => $request->request->get('reclaNroDocIdent'),
                            "reclaTelefono" => $request->request->get('reclaTelefono'),
                            "reclaCel" => $request->request->get('reclaCel'),
                            "descripcion" => $request->request->get('reclaDesc'),
                        )
                    ), 'text/html'
                );

            $this->get('mailer')->send($message);
            $mensaje = "El reclamo ha sido enviado exitosamente, en la brevedad posible lo atenderemos, muchas gracias";

        }

        return $this->render('AppBundle:Site:libro_reclamacion.html.twig', array("mensaje" => $mensaje));
    }

    public function servicio_curso_pagoAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $url = $this->generateUrl('index');

            return new RedirectResponse($url);

        }

        $user_perfil = new UsuarioPerfil();
        $user_perfil = $em->getRepository('AppBundle:UsuarioPerfil')->findOneByUser($user);

        $curso = $em->getRepository('AppBundle:Conferencia')->find($id);

        // $nombre_completo = '';
        if($user_perfil){

            $name = $user_perfil->getNombre();
            $nombre_completo = !empty($name)? $name: '';

            $last_name_dad = $user_perfil->getApellidopaterno();
            $last_name_mom = $user_perfil->getApellidomaterno();
            $nombre_completo .= " ".!empty($last_name_dad)? $last_name_dad: '';
            $nombre_completo .= " ".!empty($last_name_mom)? $last_name_mom: '';
        }

        $nombre_curso = $curso->getNombre();
        $codigo_curso = $curso->getId();
        $importe_curso = $curso->getPrecio();
        $cantidad = '1';
        $tipo_moneda = 'PEN';
        $simbolo_moneda = 'S/.';

        $form = $this->getFormSendData();

        $form->get('idcurso')->setData($codigo_curso);
        $form->get('importe')->setData($importe_curso);
        $form->get('tipo_moneda')->setData($tipo_moneda);
        $form->get('nombre_usuario')->setData($nombre_completo);

        return $this->render("AppBundle:Site:servicio_curso_payment.html.twig", array(
            'nombre_curso' => $nombre_curso,
            'codigo_curso' => $codigo_curso,
            'importe_curso' => $importe_curso,
            'cantidad' => $cantidad,
            'simbolo_moneda' => $simbolo_moneda,
            'form' => $form->createView()
            ));
    }

    private function getFormSendData()
    {
        return  $form = $this->createFormBuilder()
                    ->setAction($this->generateUrl('payment_envio'))
                    ->add('idcurso', 'hidden')
                    ->add('importe', 'hidden')
                    ->add('tipo_moneda', 'hidden')
                    ->add('nombre_usuario', 'hidden')
                    ->add('save', 'submit', array('label' => 'PAGAR'))
                    ->getForm();
    }

    public function cap_programa_tallerAction(Request $request)
    {
        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $dql   = "SELECT a FROM KayueWordpressBundle:Post a WHERE a.type='post' and a.status='publish' ORDER BY a.date DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');

        $relatedQ =  $em->createQueryBuilder()
            ->from('KayueWordpressBundle:post', 'post')
            ->select('post')
            ->leftJoin('post.taxonomies', 'tax')
            ->leftJoin('tax.term', 'term')
            ->andWhere('tax.name = :tax')->andWhere('term.slug = :term')
            ->andWhere('post.status = \'publish\'')
            ->setParameter('tax', 'category')
            ->setParameter('term', 'gestion-del-tiempo')
            ->getQuery();

        $related = $paginator->paginate(
            $relatedQ,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        return $this->render('AppBundle:Site:cap_programa_taller.html.twig', array(
            "posts" => $posts,
            "relatedPosts" => $related,
            'wp_url' => $this->container->getParameter('wp_url'),
        ));
    }

    public function termino_condicionAction()
    {
        return $this->render('AppBundle:Site:termino_condicion.html.twig');
    }


    public function culturaClimaAction(){

        return $this->forward('AppBundle:Consultoria:clima');

    }
}
