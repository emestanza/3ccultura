<?php
/**
 * Created by PhpStorm.
 * User: enmanuel
 * Date: 7/8/15
 * Time: 11:17 PM
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Orderly\PayPalIpnBundle\Ipn;
use Orderly\PayPalIpnBundle\Event as Events;

use AppBundle\Entity\Orden;
use AppBundle\Entity\OrdenDetalle;
use Symfony\Component\Validator\Constraints\DateTime;

class PaypalController extends Controller
{

    public $paypal_ipn;

    public function receiveAction(Request $request)
    {

        //getting ipn service registered in container
        $this->paypal_ipn = $this->get('orderly_pay_pal_ipn');

        //validate ipn (generating response on PayPal IPN request)
        if ($this->paypal_ipn->validateIPN()) {

            // Succeeded, now let's extract the order
            $this->paypal_ipn->extractOrder();

            // And we save the order now (persist and extract are separate because you might only want to persist the order in certain circumstances).
            $this->paypal_ipn->saveOrder();

            // Now let's check what the payment status is and act accordingly
            if ($this->paypal_ipn->getOrderStatus() == Ipn::PAID) {

                $em = $this->getDoctrine()->getManager();
                $securityContext = $this->container->get('security.context');
                $user = $securityContext->getToken()->getUser();

                $orden = new Orden();
                $orden->setUser($user);

                $fecha_trans = new \DateTime('now');
                $orden->setFecharegistro($fecha_trans);
                $orden->setFechavcto($fecha_trans);
                $orden->setMetodoPago('Paypal');
                $orden->setEstadoPago('Pagado');
                $orden->setTipoMoneda('USD');
                $orden->setImporte('25');

                $conferencia = $em->getRepository('AppBundle:Conferencia')->find(1);

                if (!$conferencia) {
                    $url = $this->generateUrl('index');
                    return new RedirectResponse($url);
                }

              /*  $orden_detalle = new OrdenDetalle();
                $orden_detalle->setConferencia($conferencia);
                $orden_detalle->setCantidad('1');
                $orden_detalle->setPrecio('9.70');
                $orden_detalle->setOrden($orden);

                $sessionObj = $this->get('session');

                $orden_detalle->setExteriorPais($sessionObj->get("exteriorPais"));
                if($sessionObj->get("exteriorPais") != "PE"){
                    $orden_detalle->setExteriorDireccion($sessionObj->get('exteriorDirecc'));
                    $orden_detalle->setExteriorEmpresa($sessionObj->get('exteriorNombre'));
                }
                else{
                    if ($sessionObj->get("boletaNombreCompleto") != ""){
                        $orden_detalle->setBoletaDni($sessionObj->get('boletaDni'));
                        $orden_detalle->setBoletaNombre($sessionObj->get('boletaNombreCompleto'));
                    }
                    else{
                        $orden_detalle->setFacturaDireccion($sessionObj->get('facturaDirecc'));
                        $orden_detalle->setFacturaRazonSocial($sessionObj->get('facturaRazonSocial'));
                        $orden_detalle->setFacturaRuc($sessionObj->get('facturaRUC'));
                    }
                }*/

                $em = $this->getDoctrine()->getManager();
                $em->persist($orden);
              //  $em->persist($orden_detalle);
                $em->flush();

                /* HEALTH WARNING:
                 *
                 * Please note that this PAID block does nothing. In other words, this controller will not respond to a successful order
                 * with any notification such as email or similar. You will have to identify paid orders by checking your database.
                 *
                 * If you want to send email notifications on successful receipt of an order, please see the alternative, Twig template-
                 * based example controller: TwigEmailNotification.php
                 */

                $documento = $this->container->getParameter('kernel.root_dir').'/../web/assets/recomendaciones_old.pdf';
                $documento2 = $this->container->getParameter('kernel.root_dir').'/../web/assets/material_para_participantes.pdf';

                $message = \Swift_Message::newInstance()
                    ->setSubject('Confirmación de Pago')
                    ->setFrom('ayuda@3ccultura.com', "3c Cultura")
                    ->setTo($this->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                            'AppBundle:Payment:email.txt.twig', array("baseUrl"=> $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath(), "orden" => $orden)
                        ), 'text/html'
                    )
                    ->attach(\Swift_Attachment::fromPath($documento))
                    ->attach(\Swift_Attachment::fromPath($documento2));

                $this->get('mailer')->send($message);

                $this->triggerEvent(Events\PayPalEvents::RECEIVED);
                return $this->render("AppBundle:Payment:payment_resultado_paypal.html.twig", array(
                    'num_pedido' => $this->paypal_ipn->getOrder()->getTxnId(),
                    'importe' => $this->paypal_ipn->getOrder()->getMcGross(),
                    'resultado' => $this->paypal_ipn->getOrderStatus()== Ipn::PAID ? 1:0,
                    'txt_respuesta' => "",
                    'cod_autoriza' => $this->paypal_ipn->getOrder()->getPayerId(),
                    'mensaje' => "",
                    'fecha_transaccion' => $this->paypal_ipn->getOrder()->getPaymentDate(),
                    'nombre_usuario' => $this->paypal_ipn->getOrder()->getAddressName(),
                ));
            }

        }
        else{
            $this->triggerEvent(Events\PayPalEvents::RECEIVED);
            return $this->render("AppBundle:Payment:payment_resultado_paypal.html.twig", array(
                'num_pedido' => "",
                'importe' => "25",
                'resultado' => 0,
                'txt_respuesta' => "",
                'cod_autoriza' => "",
                'mensaje' => "Paypal no recibió el pago por parte del usuario",
                'fecha_transaccion' => new \DateTime('now'),
                'nombre_usuario' => "",
            ));

        }
    }

    private function triggerEvent($event_name)
    {
        $dispatcher = $this->container->get('event_dispatcher');
        $dispatcher->dispatch($event_name, new Events\PayPalEvent($this->paypal_ipn));
    }
}