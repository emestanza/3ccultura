<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\RedirectResponse;

use AppBundle\Form\UsuarioPerfilType;
use AppBundle\Form\UserNewType;
use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use AppBundle\Entity\UsuarioPerfil;
use AppBundle\Entity\Orden;


class RegistroController extends Controller
{

    public function registro_loginAction(Request $request)
    {

        $postData = $request->request->get('fromBanner');

        if ($postData != "1"){

            $securityContext = $this->container->get('security.context');
            $router = $this->container->get('router');
            if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                $redirect_url = 'index';
                return new RedirectResponse($router->generate($redirect_url));
            }
        }

        // Validar si se envio algun id de curso para redirigir
        // hacia el detalle de pago del curso
        $idCurso = $request->query->get('id');

        if (isset($idCurso) && !empty($idCurso)) {
            $request->getSession()->getFlashBag()->add('registration_done', 'done');
            $redirigir_url = $this->generateUrl('cart_add_item', array('id' => 1));
        } else {
            $redirigir_url = $this->generateUrl('conferencia_virtual');
        }

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');

        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return new RedirectResponse($redirigir_url);
        }

        $user_register = new UsuarioPerfil();
        $form = $this->createFormRegister($user_register);
        $form->handleRequest($request);

        $csrfToken = $this->container->has('form.csrf_provider')
            ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate')
            : null;

        $isValid = $request->isMethod('GET');

        if ($form->isValid()) {
            $isValid = true;
            $tipo_usuario = 'Persona';
            $userManager = $this->get('fos_user.user_manager');

            $user = $user_register->getUser();

            $email = $user->getEmail();
            $role = array('ROLE_USER');

            $user->setUsername($email);
            $user->setEnabled(true);
            $user->setRoles($role);

            $userManager->updateUser($user);

            $user_register->setUser($user);
            $user_register->setTipoUsuario($tipo_usuario);
            $user_register->setApellidopaterno("");

            $em = $this->getDoctrine()->getManager();
            $em->persist($user_register);
            $em->flush();

            $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
            $this->get('security.context')->setToken($token);
            $this->get('session')->set('_security_main', serialize($token));
            $this->get('session')->getFlashBag()->add('registro_success', 'Ud. se registro correctamente!');

            if ($postData == "1"){
                $documento = $this->container->getParameter('kernel.root_dir').'/../web/assets/encuesta.pdf';

                // Attach it to the message
                $message = \Swift_Message::newInstance()
                    ->setSubject('Gracias por Registrarte! - Encuesta gratis')
                    ->setFrom('ayuda@3ccultura.com', "3c Cultura")
                    ->setTo("enmanuel.mestanza@webandlogics.com")
                    ->setBody(
                        $this->renderView(
                            'AppBundle:Conferencia:email.txt.twig'
                        ), 'text/html'
                    )
                    ->attach(\Swift_Attachment::fromPath($documento));

                $this->get('mailer')->send($message);
                $this->get('session')->getFlashBag()->add('registro_success', 'Gracias por registrate, recibirás un correo electrónico donde obtendrás tu encuesta!');
                return $this->forward("AppBundle:Conferencia:index",array(), array('registroSuccess' => true));
           }

           return new RedirectResponse($redirigir_url);

        }
       /* else{
            $isValid = false;
        }*/

     //   var_dump($isValid);
     //die();
        return $this->render("AppBundle:Registro:registro_login.html.twig", array(
            'user_register' => $user_register,
            'form' => $form->createView(),
            'form_full' => $form,
            'csrf_token' => $csrfToken,
            'url_registrar' => $redirigir_url,
            "valid" => $isValid
        ));

    }


    /**
     * Crear formulario para registrar un nuevo usuario
     *
     * @param User $entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFormRegister(UsuarioPerfil $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new UsuarioPerfilType($securityContext, $em), $usuario, array(
            'method' => 'POST',
        ));

        $form->add('save', 'submit', array('label' => 'Regístrate', 'attr' => array('class' => 'btn-default btn-inscrib')));

        return $form;
    }

    /**
     * Crear formulario para registrar un nuevo usuario
     *
     * @param User $entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFormLogin(User $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new UserType($securityContext, $em), $usuario, array(
            'action' => $this->generateUrl('fos_user_security_check'),
            'method' => 'POST',
        ));

        $form->add('ingresar', 'submit', array('label' => 'INICIAR SESIÓN', 'attr' => array('class' => 'btn-default')));

        return $form;
    }


}