<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Entity\UsuarioPerfil;
use Lsw\ApiCallerBundle\Call\HttpGetJson;

class CartController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if (!$securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $url = $this->generateUrl('curso_registro');
            return new RedirectResponse($url);
        }

        $session = $this->getRequest()->getSession();
        $cart = $session->get('cart', array());

        if ($cart != '') {

            $user_perfil = new UsuarioPerfil();
            $user_perfil = $em->getRepository('AppBundle:UsuarioPerfil')->findOneByUser($user);

            $nombre_completo = '';
            if ($user_perfil) {

                $name = $user_perfil->getNombre();
                $nombre_completo = !empty($name) ? $name : '';

                $last_name_dad = $user_perfil->getApellidopaterno();
                $last_name_mom = $user_perfil->getApellidomaterno();
                $nombre_completo .= " " . !empty($last_name_dad) ? $last_name_dad : '';
                $nombre_completo .= " " . !empty($last_name_mom) ? $last_name_mom : '';
            }

            $cantidad = '1';
            $tipo_moneda = 'PEN';
            $simbolo_moneda = 'S/.';

            $form = $this->getFormSendData();

            $request->getSession()->getFlashBag()->add('course_done', 'done');
            //var_dump($this->container->get('request')->getClientIp()); die();

            return $this->render("AppBundle:Cart:index.html.twig", array(
                'nombre_curso' => '',
                'codigo_curso' => '',
                'importe_curso' => '',
                'cantidad' => $cantidad,
                'simbolo_moneda' => $simbolo_moneda,
                'carts' => $cart,
                'form' => $form->createView()
            ));

        } else {
            return $this->render('AppBundle:Cart:index.html.twig', array(
                'empty' => true,
            ));
        }
    }

    private function getFormSendData()
    {
        return $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('payment_envio'))
            //*->add('save', 'submit', array('label' => 'PAGAR'))*/
            ->getForm();
    }


    public function add_itemAction($id, Request $request)
    {
        $session = $this->getRequest()->getSession();
        $cart = $cart = $session->get('cart', array());
        $session->set('cart', '');

        $em = $this->getDoctrine()->getManager();
        $conferencia = $em->getRepository('AppBundle:Conferencia')->find($id);

        if (!$conferencia) {
            return new Response('This product is not available in Stores');
        }

        if (!$cart) {

            $session->set('cart', array(
                'id_conferencia' => $conferencia->getId(),
                'producto' => $conferencia->getNombre(),
                'cantidad' => 1,
                'precio' => $conferencia->getPrecio(),
                'uri' => $conferencia->getUri(),
            ));
        } else {
            $cart['id_conferencia'] = $conferencia->getId();
            $cart['producto'] = $conferencia->getNombre();
            $cart['cantidad'] = 1;
            $cart['precio'] = $conferencia->getPrecio();
            $cart['uri'] = $conferencia->getUri();
            $session->set('cart', $cart);
        }

        return $this->redirect($this->generateUrl('cart'));
    }

    public function saveSessionDocumentAction(Request $request){

        //echo "fino"; die();

        /**
         * exteriorPais=DZ&
         * boletaNombreCompleto=&
         * boletaDni=&
         * facturaRazonSocial=&
         * facturaRUC=&
         * facturaDirecc=&
         * exteriorNombre=sffsd&
         * exteriorDirecc=fdfd&

         */
        $sessionObj = $this->get('session');
        $sessionObj->set("exteriorPais", $request->request->get("exteriorPais"));

        if($request->request->get("exteriorPais") != "PE"){

            $sessionObj->set("exteriorNombre", $request->request->get("exteriorNombre"));
            $sessionObj->set("exteriorDirecc", $request->request->get("exteriorDirecc"));
        }
        else{

           // var_dump($request->request->get("exteriorPais"));
            //die();

            if ($request->request->get("boletaNombreCompleto") != ""){
                $sessionObj->set("boletaNombreCompleto", $request->request->get("boletaNombreCompleto"));
                $sessionObj->set("boletaDni", $request->request->get("boletaDni"));
            }
            else{
                $sessionObj->set("facturaRazonSocial", $request->request->get("facturaRazonSocial"));
                $sessionObj->set("facturaRUC", $request->request->get("facturaRUC"));
                $sessionObj->set("facturaDirecc", $request->request->get("facturaDirecc"));
            }
        }

        echo "success"; die();


        /**
         * $paymentOption = $request->request->get('paymentOption');

        if ($paymentOption == "boleta") {
        $orden_detalle->setBoletaDni($request->request->get('boletaDni'));
        $orden_detalle->setBoletaNombre($request->request->get('boletaNombreCompleto'));
        }

        if ($paymentOption == "factura") {
        $orden_detalle->setFacturaDireccion($request->request->get('facturaDirecc'));
        $orden_detalle->setFacturaRazonSocial($request->request->get('facturaRazonSocial'));
        $orden_detalle->setFacturaRuc($request->request->get('facturaRUC'));
        }

        if ($paymentOption == "exterior") {
        $orden_detalle->setExteriorDireccion($request->request->get('exteriorDirecc'));
        $orden_detalle->setExteriorEmpresa($request->request->get('exteriorNombre'));
        $orden_detalle->setExteriorPais($request->request->get('exteriorPais'));

        }

        $em = $this->getDoctrine()->getManager();

        $em->persist($orden);
        $em->persist($orden_detalle);
        $em->flush();
         */
    }

}