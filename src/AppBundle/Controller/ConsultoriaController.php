<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ConsultoriaController extends Controller
{
    public function indexAction()
    {
        return $this->render('.html.twig');
    }

    public function cultura_organizacionalAction(Request $request)
    {
        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $dql   = "SELECT a FROM KayueWordpressBundle:Post a WHERE a.type='post' and a.status='publish' ORDER BY a.date DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );


       $relatedQ =  $em->createQueryBuilder()
        ->from('KayueWordpressBundle:post', 'post')
        ->select('post')
        ->leftJoin('post.taxonomies', 'tax')
        ->leftJoin('tax.term', 'term')
        ->andWhere('tax.name = :tax')->andWhere('term.slug = :term')
        ->andWhere('post.status = \'publish\'')
        ->setParameter('tax', 'category')
        ->setParameter('term', 'cultura-organizacional')
        ->getQuery();

        $related = $paginator->paginate(
            $relatedQ,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        return $this->render('AppBundle:Consultoria:cultura_organizacional.html.twig',
            array(
                "posts" => $posts,
                "relatedPosts" => $related,
                'wp_url' => $this->container->getParameter('wp_url'),
            ));
    }


    public function climaAction(Request $request) {

        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $dql   = "SELECT a FROM KayueWordpressBundle:Post a WHERE a.type='post' and a.status='publish' ORDER BY a.date DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );


        $relatedQ =  $em->createQueryBuilder()
            ->from('KayueWordpressBundle:post', 'post')
            ->select('post')
            ->leftJoin('post.taxonomies', 'tax')
            ->leftJoin('tax.term', 'term')
            ->andWhere('tax.name = :tax')->andWhere('term.slug = :term')
            ->andWhere('post.status = \'publish\'')
            ->setParameter('tax', 'category')
            ->setParameter('term', 'clima-laboral')
            ->getQuery();

        $related = $paginator->paginate(
            $relatedQ,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        return $this->render("AppBundle:Consultoria:clima_laboral.html.twig", array(
            "posts" => $posts,
            "relatedPosts" => $related,
            'wp_url' => $this->container->getParameter('wp_url'),
        ));
    }

    public function liderazgoAction(Request $request) {
        $entityManagerName = $this->container->getParameter('kayue_wordpress.entity_manager');
        $em    = $this->get('doctrine.orm.'.$entityManagerName.'_entity_manager');
        $dql   = "SELECT a FROM KayueWordpressBundle:Post a WHERE a.type='post' and a.status='publish' ORDER BY a.date DESC";
        $query = $em->createQuery($dql);

        $paginator  = $this->get('knp_paginator');
        $posts = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );


        $relatedQ =  $em->createQueryBuilder()
            ->from('KayueWordpressBundle:post', 'post')
            ->select('post')
            ->leftJoin('post.taxonomies', 'tax')
            ->leftJoin('tax.term', 'term')
            ->andWhere('tax.name = :tax')->andWhere('term.slug = :term')
            ->andWhere('post.status = \'publish\'')
            ->setParameter('tax', 'category')
            ->setParameter('term', 'management')
            ->getQuery();

        $related = $paginator->paginate(
            $relatedQ,
            $request->query->get('page', 1)/*page numpber*/,
            4/*limit per page*/
        );

        return $this->render("AppBundle:Consultoria:liderazgo.html.twig", array(
            "posts" => $posts,
            "relatedPosts" => $related,
            'wp_url' => $this->container->getParameter('wp_url'),
        ));


    }


    public function sendInscAction(Request $request) {


    }
}