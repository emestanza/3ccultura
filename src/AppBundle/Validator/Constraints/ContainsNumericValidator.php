<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsNumericValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $count = strlen($value);

        if($count > 0) {

            if (!preg_match('/^[0-9]+$/', $value, $matches)) {
                $this->context->addViolation(
                    $constraint->message,
                    array('%string%' => $value)
                );
            }

        }
    }
}
