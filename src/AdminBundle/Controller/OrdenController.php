<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AdppBundle\Entity\Orden;
use AdminBundle\Form\OrdenType;

/**
 * Orden controller.
 *
 */
class OrdenController extends Controller
{

    /**
     * Lists all Orden entities.
     *
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $conferencias = $em->getRepository('AppBundle:Conferencia')->findAll();

        return $this->render('AdminBundle:Orden:index.html.twig', array(
            "conferencias" => $conferencias
        ));
    }

    /**
     * Mostrar "Lista de Ordenes"
     *
     */
    public function listaOrdenesAction()
    {

        $datos = '';

        $conferancias = $this->getOrdenesByFilter();

        foreach ($conferancias as $key => $value) {

            $estado_html = '<div class="text-center">';

            if($value['estado_pago'] == 'Pagado'){
                $estado_html .= '<small class="label bg-green">Pagado</small>';
            }
            else
            {
                $estado_html .= '<small class="label bg-red">'.$value['estado_pago'].'</small>';
            }

            $estado_html .= '</div">';

            $url_bloquear =  $this->generateUrl('ordendetalle', array('uid' => $value['uid'],'oid' => $value['id']));

            $accion_html = '<div class="acciones">';
            $accion_html .= ' <div class="btn-group">';
            $accion_html .= ' <button class="btn btn-default btn-ver btn-sm" data-url="'.$url_bloquear.'" data-target="#modalContent">';
            $accion_html .= ' <i class="fa fa-file-text-o"></i>';
            $accion_html .= ' </button>';
            $accion_html .= ' </div>';
            $accion_html .= ' </div>';

            $data['key'] = $key+1;
            $data['fecharegistro'] = empty($value['fecharegistro'])? '-----': $value['fecharegistro']->format('m/d/Y H:m') ;
            $data['fechavcto'] = empty($value['fechavcto'])? '-----': $value['fechavcto']->format('m/d/Y H:m') ;
            $data['fechapago'] = empty($value['fechapago'])? '-----': $value['fechapago']->format('m/d/Y H:m') ;
            $data['metodopago'] = empty($value['metodopago'])? '-----': $value['metodopago'];
            $data['codigo_pago'] = empty($value['codigo_pago'])? '-----': $value['codigo_pago'];
            $data['estado_pago'] = $estado_html;
            $data['tipo_moneda'] = empty($value['tipo_moneda'])? '-----': $value['tipo_moneda'];
            $data['certificado'] = empty($value['certificado'])? '-----': $value['certificado'];
            $data['importe'] = empty($value['importe'])? '-----': $value['importe'];
            $data['acciones'] = $accion_html;
            
            $datos[] = $data;
        }

        $respuesta = array("data"=>$datos);

        return new JsonResponse($respuesta);
    }

    public function getOrdenesByFilter()
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $fields = array(
            'c.id',
            'u.id as uid',
            'c.fecharegistro',
            'c.fechavcto',
            'c.fechapago', 
            'c.metodopago', 
            'c.codigo_pago', 
            'c.estado_pago', 
            'c.tipo_moneda', 
            'c.certificado', 
            'c.importe'
            );

        $nombre = '';
        $empresa = '';
        $estado = '';

        $qb = $this->getQueryOrdenesByFilter();

        $qb->select($fields);
        $qb->orderBy('c.id', 'DESC');

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getQueryOrdenesByFilter()
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $qb
            ->from('AppBundle:Orden', 'c')
            ->innerjoin('AppBundle:User', 'u', 'WITH','u.id = c.user')
            ;

        return $qb;
    }

    /**
     * Creates a new Orden entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Orden();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('orden_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:Orden:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Orden entity.
     *
     * @param Orden $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Orden $entity)
    {
        $form = $this->createForm(new OrdenType(), $entity, array(
            'action' => $this->generateUrl('orden_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Orden entity.
     *
     */
    public function newAction()
    {
        $entity = new Orden();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:Orden:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Orden entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Orden')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Orden:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Orden entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Orden')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Orden:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Orden entity.
    *
    * @param Orden $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Orden $entity)
    {
        $form = $this->createForm(new OrdenType(), $entity, array(
            'action' => $this->generateUrl('orden_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Orden entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Orden')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('orden_edit', array('id' => $id)));
        }

        return $this->render('AdminBundle:Orden:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Orden entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminBundle:Orden')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Orden entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('orden'));
    }

    /**
     * Creates a form to delete a Orden entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('orden_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

}


