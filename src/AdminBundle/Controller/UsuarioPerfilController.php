<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\UsuarioPerfil;
use AppBundle\Entity\User;
use AdminBundle\Form\UsuarioPerfilType;

/**
 * UsuarioPerfil controller.
 *
 */
class UsuarioPerfilController extends Controller
{

    /**
     * Lists all UsuarioPerfil entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:UsuarioPerfil')->findAll();

        return $this->render('AdminBundle:UsuarioPerfil:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Mostrar "Lista de Usuarios"
     *
     */
    public function listaUsuariosAction()
    {

        $datos = '';

        $usuarios = $this->getUsuariosByFilter();

        foreach ($usuarios as $key => $value) {

            if (!empty($value['path'])) {
                $logo_html = '<span class="logo-table" style = "background-image: ';
                $logo_html .= ' url( /uploads/documents/'.$value['path'].' )">';
                $logo_html .= '</span>';
            }
            else{

                $logo_html = '<span class="logo-table" ></span>';
            }

            $estado_html = '<div class="text-center">';

            if($value['estado'] == 1){
                $estado_html .= '<small class="label bg-green">Activo</small>';
            }
            else
            {
                $estado_html .= '<small class="label bg-red">Bloqueado</small>';
            }

            $estado_html .= '</div">';

            $url_editar =  $this->generateUrl('usuarioperfil_edit', array('id' => $value['id']));
            $url_bloquear =  $this->generateUrl('usuarioperfil_confirm', array('id' => $value['id']));

            $accion_html = '<div class="acciones">';
            $accion_html .= ' <div class="btn-group">';
            $accion_html .= ' <a class="btn btn-default btn-sm" href="'.$url_editar.'" >';
            $accion_html .= ' <i class="fa fa-edit"></i>';
            $accion_html .= ' </a>';
            $accion_html .= ' <button class="btn btn-default btn-eliminar btn-sm" data-url="'.$url_bloquear.'" data-target="#modalContent">';
            $accion_html .= ' <i class="fa fa-trash-o"></i>';
            $accion_html .= ' </button>';
            $accion_html .= ' </div>';
            $accion_html .= ' </div>';

            $data['key'] = $key+1;
            $data['imagen'] = $logo_html;
            $data['nombre'] = empty($value['nombre'])? '-----': $value['nombre'] ;
            $data['email'] = empty($value['email'])? '-----': $value['email'] ;
            $data['telefonomovil'] = empty($value['telefonomovil'])? '-----': $value['telefonomovil'] ;
            $data['tipo_usuario'] = empty($value['tipo_usuario'])? '-----': $value['tipo_usuario'] ;
            $data['dni_ruc'] = empty($value['dni_ruc'])? '-----': $value['dni_ruc'];
            $data['estado'] = $estado_html;
            $data['accion'] = $accion_html;

            $datos[] = $data;
        }

        $respuesta = array('data'=>$datos);

        return new JsonResponse($respuesta);
    }

    public function getUsuariosByFilter()
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $fields = array('u.username',
                        'u.email',
                        'u.id', 
                        'p.nombre',
                        'p.telefonomovil',
                        'p.tipo_usuario',
                        'p.dni_ruc',
                        'f.path',
                        'u.locked as estado'
                        );

        $qb = $this->getQueryUsuariosByFilter();

        $qb->select($fields);
        $qb->orderBy('estado', 'ASC');
        $qb->addOrderBy('p.nombre', 'ASC');
        $qb->addOrderBy('u.email', 'ASC');

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getQueryUsuariosByFilter()
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $qb
            ->from('AppBundle:User', 'u')
            ->join('AppBundle:UsuarioPerfil', 'p','WITH','p.user = u.id')
            ->leftjoin('AppBundle:File', 'f', 'WITH','p.fid_img = f.id')
            ;
        return $qb;
    }

    /**
     * Creates a new UsuarioPerfil entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new UsuarioPerfil();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('usuarioperfil_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminBundle:UsuarioPerfil:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a UsuarioPerfil entity.
     *
     * @param UsuarioPerfil $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(UsuarioPerfil $entity, $securityContext, $em)
    {
        $form = $this->createForm(new UsuarioPerfilType($securityContext, $em), $entity, array(
            'action' => $this->generateUrl('usuarioperfil_new'),
            'method' => 'POST',
        ));

        $form->add('save', 'submit', array('label' => 'Guardar', 'attr'=>array('class'=>'btn-default')));

        return $form;
    }

    /**
     * Displays a form to create a new UsuarioPerfil entity.
     *
     */
    public function newAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');

        $entity = new UsuarioPerfil();
        $form   = $this->createCreateForm($entity, $securityContext, $em);
       
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('mensaje', 'Datos guardados correctamente!');
            
            return $this->redirect($this->generateUrl('usuarioperfil'));

        }

        $logo_url = '';
        return $this->render('AdminBundle:UsuarioPerfil:new.html.twig', array(
            'entity' => $entity,
            'logo_url' => $logo_url,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a UsuarioPerfil entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:UsuarioPerfil')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UsuarioPerfil entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:UsuarioPerfil:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing UsuarioPerfil entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $entity = $em->getRepository('AppBundle:UsuarioPerfil')->findByUser($user);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UsuarioPerfil entity.');
        }

        $fid_logo = $entity[0]->getFidImg();
        $logo_url = '';

        if($fid_logo){
            $foto = $em->getRepository('AppBundle:File')->find($fid_logo);
            if($foto){
                $logo_url = $foto->getWebPath();
            }
        }

        $editForm = $this->createEditForm($entity[0], $securityContext, $em);

        return $this->render('AdminBundle:UsuarioPerfil:new.html.twig', array(
            'entity'      => $entity,
            'logo_url' => $logo_url,
            'form'   => $editForm->createView()
        ));
    }

    /**
    * Creates a form to edit a UsuarioPerfil entity.
    *
    * @param UsuarioPerfil $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(UsuarioPerfil $entity, $securityContext, $em)
    {
        $form = $this->createForm(new UsuarioPerfilType($securityContext, $em), $entity, array(
            'action' => $this->generateUrl('usuarioperfil_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('save', 'submit', array('label' => 'Actualizar', 'attr'=>array('class'=>'btn-default')));

        return $form;
    }

    public function confirmAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        
        $entity = new UsuarioPerfil();

        $entity = $em->getRepository('AppBundle:Conferencista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conferencista entity.');
        }

        $confirmForm   = $this->createDeleteForm($id);

        return $this->render('AdminBundle:UsuarioPerfil:confirm.html.twig', array(
            'form'   => $confirmForm->createView(),
            'entity'   => $entity
        ));
    }

    /**
     * Deletes a UsuarioPerfil entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UsuarioPerfil entity.');
            }

            $locked = $entity->isAccountNonLocked();

            if($locked){
                $entity->setLocked(true);
            }
            else{
                $entity->setLocked(false);
            }

            $em->persist($entity);
            $em->flush();

            $respuesta = array('status' => 'eliminar', 'msj'=> 'Datos actualizados corectamente.' );
            return new JsonResponse($respuesta);
        }

        return $this->redirect($this->generateUrl('usuarioperfil'));
    }

    /**
     * Creates a form to delete a UsuarioPerfil entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuarioperfil_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('eliminar', 'submit', array('label' => 'Aceptar', 'attr'=>array('class'=>'btn-default')))
            ->add('cancelar', 'button', array('label' => 'Cancelar', 'attr'=>array('class'=>'btn-default')))
            ->getForm()
        ;
    }
}
