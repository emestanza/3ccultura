<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Conferencia;
use AdminBundle\Form\ConferenciaType;

/**
 * Conferencia controller.
 *
 */
class ConferenciaController extends Controller
{

    /**
     * Lists all Conferencia entities.
     *
     */
    public function indexAction()
    {

        return $this->render('AdminBundle:Conferencia:index.html.twig', array(
        ));
    }

    /**
     * Mostrar "Lista de Conferencias"
     *
     */
    public function listaConferenciasAction()
    {

        $datos = '';

        $conferancias = $this->getConferenciasByFilter();

        foreach ($conferancias as $key => $value) {

            if (!empty($value['path'])) {
                $logo_html = '<span class="logo-table" style = "background-image: ';
                $logo_html .= ' url( /uploads/documents/'.$value['path'].' )">';
                $logo_html .= '</span>';
            }
            else{

                $logo_html = '<span class="logo-table" ></span>';
            }

            $estado_html = '<div class="text-center">';
            if($value['estado'] == 1){
                $estado_html .= '<small class="label bg-green">Activo</small>';
            }
            else
            {
                $estado_html .= '<small class="label bg-red">Bloqueado</small>';
            }

            $estado_html .= '</div">';

            $url_editar =  $this->generateUrl('conferencia_edit', array('id' => $value['id']));
            $url_bloquear =  $this->generateUrl('conferencia_confirm', array('id' => $value['id']));

            $accion_html = '<div class="acciones">';
            $accion_html .= ' <div class="btn-group">';
            $accion_html .= ' <a class="btn btn-default btn-sm" href="'.$url_editar.'">';
            $accion_html .= ' <i class="fa fa-edit"></i>';
            $accion_html .= ' </a>';
            $accion_html .= ' <button class="btn btn-default btn-eliminar btn-sm" data-url="'.$url_bloquear.'" data-target="#modalContent">';
            $accion_html .= ' <i class="fa fa-trash-o"></i>';
            $accion_html .= ' </button>';
            $accion_html .= ' </div>';
            $accion_html .= ' </div>';

            $data['key'] = $key+1;
            $data['imagen'] = $logo_html;
            $data['nombre'] = empty($value['nombre'])? '-----': $value['nombre'] ;
            //$data['descripcion'] = empty($value['descripcion'])? '-----': $value['descripcion'] ;
            $data['precio'] = empty($value['precio'])? '-----': $value['precio'] ;
            $data['fechainicio'] = empty($value['fecha_inicio'])? '-----': $value['fecha_inicio']->format('m/d/Y') ;
            $data['duracion'] = empty($value['duracion'])? '-----': $value['duracion'] ;
            $data['estado'] = $estado_html;
            $data['accion'] = $accion_html;

            $datos[] = $data;
        }

        $respuesta = array("data"=>$datos);

        return new JsonResponse($respuesta);
    }

    public function getConferenciasByFilter()
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $fields = array(
            'c.nombre',
            'c.descripcion',
            'c.id', 
            'c.precio', 
            'c.fecha_inicio', 
            'c.duracion', 
            'c.uri', 
            'c.estado', 
            'f.path'
            );

        $nombre = '';
        $empresa = '';
        $estado = '';

        $qb = $this->getQueryConferenciasByFilter();

        $qb->select($fields);
        $qb->orderBy('c.id', 'DESC');

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getQueryConferenciasByFilter()
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $qb
            ->from('AppBundle:Conferencia', 'c')
            ->leftjoin('AppBundle:File', 'f', 'WITH','c.fid_img = f.id')
            ;

        return $qb;
    }

    /**
     * Creates a form to create a Conferencia entity.
     *
     * @param Conferencia $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Conferencia $entity, $securityContext, $em)
    {
        $form = $this->createForm(new ConferenciaType($securityContext, $em), $entity, array(
            'action' => $this->generateUrl('conferencia_new'),
            'method' => 'POST',
        ));

        $form->add('save', 'submit', array('label' => 'Guardar', 'attr'=>array('class'=>'btn-default')));

        return $form;
    }

    /**
     * Displays a form to create a new Conferencia entity.
     *
     */
    public function newAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');

        $entity = new Conferencia();
        $form   = $this->createCreateForm($entity, $securityContext, $em);
       
        $form->handleRequest($request);

        if ($form->isValid()) {

            $diff = $this->formatDateDiff($entity->getFechaFin(), $entity->getFechaInicio());

            $entity->setDuracion($diff);

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('mensaje', 'Datos guardados correctamente!');

            return $this->redirect($this->generateUrl('conferencia'));
        }

        $logo_url = '';
        
        return $this->render('AdminBundle:Conferencia:new.html.twig', array(
            'entity' => $entity,
            'logo_url' => $logo_url,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Conferencia entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Conferencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conferencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Conferencia:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Conferencia entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        
        $entity = $em->getRepository('AppBundle:Conferencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conferencia entity.');
        }

        $fid_logo = $entity->getFidImg();
        $logo_url = '';

        if($fid_logo){
            $foto = $em->getRepository('AppBundle:File')->find($fid_logo);
            if($foto){
                $logo_url = $foto->getWebPath();
            }
        }

        $editForm = $this->createEditForm($entity, $securityContext, $em);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('mensaje', 'Datos guardados correctamente!');

            return $this->redirect($this->generateUrl('conferencia'));
        }

        return $this->render('AdminBundle:Conferencia:new.html.twig', array(
            'entity'      => $entity,
            'logo_url' => $logo_url,
            'form'   => $editForm->createView()
        ));
    }

    /**
    * Creates a form to edit a Conferencia entity.
    *
    * @param Conferencia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Conferencia $entity, $securityContext, $em)
    {
        $form = $this->createForm(new ConferenciaType( $securityContext, $em), $entity, array(
            'action' => $this->generateUrl('conferencia_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('save', 'submit', array('label' => 'Actualizar', 'attr'=>array('class'=>'btn-default')));

        return $form;
    }
 
    public function confirmAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        
        $entity = new Conferencia();

        $entity = $em->getRepository('AppBundle:Conferencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conferencia entity.');
        }

        $confirmForm   = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Conferencia:confirm.html.twig', array(
            'form'   => $confirmForm->createView(),
            'entity'   => $entity
        ));
    }

    /**
     * Deletes a Conferencia entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Conferencia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Conferencia entity.');
            }

            if( $entity->getEstado()){
                $entity->setEstado(false);
            }
            else{
                $entity->setEstado(true);
            }

            $em->persist($entity);
            $em->flush();

            $respuesta = array('status' => 'eliminar', 'msj'=> 'Datos actualizados corectamente.' );
            return new JsonResponse($respuesta);
        }

        return $this->redirect($this->generateUrl('conferencista'));
    }

    /**
     * Creates a form to delete a Conferencia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('conferencia_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('eliminar', 'submit', array('label' => 'Aceptar', 'attr'=>array('class'=>'btn-default')))
            ->add('cancelar', 'button', array('label' => 'Cancelar', 'attr'=>array('class'=>'btn-default')))
            ->getForm()
        ;
    }

    public function formatDateDiff($start, $end=null) { 
        if(!($start instanceof \DateTime)) { 
            $start = new \DateTime($start); 
        } 
        
        if($end === null) { 
            $end = new \DateTime(); 
        } 
        
        if(!($end instanceof \DateTime)) { 
            $end = new \DateTime($start); 
        } 
        
        $interval = $end->diff($start); 
        $doPlural = function($nb,$str){return $nb>1?$str.'':$str;}; // adds plurals 
        
        $format = array(); 
        if($interval->y !== 0) { 
            $format[] = "%y".$doPlural($interval->y, "'a"); 
        } 
        if($interval->m !== 0) { 
            $format[] = "%m".$doPlural($interval->m, "'m"); 
        } 
        if($interval->d !== 0) { 
            $format[] = "%d".$doPlural($interval->d, "'d"); 
        } 
        if($interval->h !== 0) { 
            $format[] = "%h".$doPlural($interval->h, "'h"); 
        } 
        if($interval->i !== 0) { 
            $format[] = "%i".$doPlural($interval->i, "'min"); 
        } 
        if($interval->s !== 0) { 
            if(!count($format)) { 
                return "0 min"; 
            } else { 
                $format[] = "%s".$doPlural($interval->s, "'s"); 
            } 
        } 
        if(count($format) > 1) { 
            $format = array_shift($format)." ".array_shift($format); 
        } else { 
            $format = array_pop($format); 
        } 
        
        // Prepend 'since ' or whatever you like 
        return $interval->format($format); 
    } 

}
