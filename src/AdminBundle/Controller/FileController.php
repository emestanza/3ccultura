<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\File;

class FileController extends Controller
{

    public function uploadFileAction(Request $request)
    {

        $files = $request->files;
        $em = $this->get('doctrine')->getManager();
        $securityContext = $this->container->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if ($user) {
            $respuesta = array();

            foreach ($files as $uploadedFile) {

                $upload = new File();
                $upload->setFile($uploadedFile);

                try{

                    $em->persist($upload);
                    $em->flush();

                    $respuesta['status'] = 1;
                    $respuesta['msg'] = '';
                    $respuesta['data']['fid'] = $upload->getId();
                    $respuesta['data']['url'] = $upload->getWebPath();

                }catch(Exception $e){

                    $respuesta['status'] = 0;
                    $respuesta['msg'] = 'Error al cargar el archivo.';

                }

            }

        return new JsonResponse($respuesta);
        }
    }


}
