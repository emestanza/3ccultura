<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Testimonio;
use AdminBundle\Form\TestimonioType;

/**
 * Testimonio controller.
 *
 */
class TestimonioController extends Controller {

    /**
     * Lists all Testimonio entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('AppBundle:Testimonio')->findAll();

        return $this->render('AdminBundle:Testimonio:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Testimonio entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Testimonio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('testimonio'));
        }

        return $this->render('AdminBundle:Testimonio:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Testimonio entity.
     *
     * @param Testimonio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Testimonio $entity)
    {
        $form = $this->createForm(new TestimonioType(), $entity, array(
            'action' => $this->generateUrl('testimonio_create'),
            'method' => 'POST',
        ));

        //$form->add('submit', 'submit', array('label' => 'Create'));
        $form->add('save', 'submit', array('label' => 'Guardar', 'attr' => array('class' => 'btn-default')));

        return $form;
    }

    /**
     * Displays a form to create a new Testimonio entity.
     *
     */
    public function newAction()
    {
        $entity = new Testimonio();
        $form = $this->createCreateForm($entity);

        return $this->render('AdminBundle:Testimonio:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Testimonio entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Testimonio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testimonio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Testimonio:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Testimonio entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Testimonio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testimonio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Testimonio:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Testimonio entity.
     *
     * @param Testimonio $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Testimonio $entity)
    {
        $form = $this->createForm(new TestimonioType(), $entity, array(
            'action' => $this->generateUrl('testimonio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('save', 'submit', array('label' => 'Actualizar', 'attr' => array('class' => 'btn-default')));

        return $form;
    }

    /**
     * Edits an existing Testimonio entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Testimonio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testimonio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('testimonio'));
        }

        return $this->render('AdminBundle:Testimonio:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Testimonio entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        /*$form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {*/
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Testimonio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Testimonio entity.');
        }

        $em->remove($entity);
        $em->flush();
        // }

        return $this->redirect($this->generateUrl('testimonio'));
    }

    /**
     * Creates a form to delete a Testimonio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('testimonio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
}
