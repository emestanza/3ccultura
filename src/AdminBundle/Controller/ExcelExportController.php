<?php


namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ExcelExportController extends Controller {

    public function indexAction(Request $request)
    {

        $filters = array();
        if ($request->request->get('registrationDate') != "")
            $filters["o.fecharegistro"] = date("Y-m-d", strtotime($request->request->get('registrationDate')));

       /* if ($request->request->get('horarioId') != "")
            $filters["hc.id"] = $request->request->get('horarioId');*/

        if ($request->request->get('paymentState'))
            $filters["o.estado_pago"] = $request->request->get('paymentState');

        $ordenes = $this->getOrdenDetalleByFilter($filters);

        // ask the service for a Excel5
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("3ccultura")
            ->setLastModifiedBy("3ccultura")
            ->setTitle("Office 2005 XLSX Test Document")
            ->setSubject("Office 2005 XLSX Test Document")
            ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
            ->setKeywords("office 2005 openxml php")
            ->setCategory("Result file");

        $phpExcelObject->getActiveSheet()->SetCellValue('A1', 'Código Orden');
        $phpExcelObject->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setRGB('FF0000');;
        $phpExcelObject->getActiveSheet()->SetCellValue('B1', 'Método Pago');
        $phpExcelObject->getActiveSheet()->SetCellValue('C1', 'Código Pago');
        $phpExcelObject->getActiveSheet()->SetCellValue('D1', 'Estado Pago');
        $phpExcelObject->getActiveSheet()->SetCellValue('E1', 'Importe');
        $phpExcelObject->getActiveSheet()->SetCellValue('F1', 'Conferencia');
        $phpExcelObject->getActiveSheet()->SetCellValue('G1', 'email');
        $phpExcelObject->getActiveSheet()->SetCellValue('H1', 'Nombre');
        $phpExcelObject->getActiveSheet()->SetCellValue('I1', 'Télefono Movil');
        $phpExcelObject->getActiveSheet()->SetCellValue('J1', 'Dni / Ruc');
        $phpExcelObject->getActiveSheet()->SetCellValue('K1', 'Tipo Usuario');
        $phpExcelObject->getActiveSheet()->SetCellValue('L1', 'Fecha de Orden');
      //  $phpExcelObject->getActiveSheet()->SetCellValue('M1', 'Fecha de Registro');

        $data = array();
        foreach ($ordenes as $key => $value) {
            $d = array();
            $d['id'] = $value['id'];
            $d['metodopago'] = $value['metodopago'];
            $d['codigo_pago'] = " " . $value['codigo_pago'];
            $d['estado_pago'] = $value['estado_pago'];
            $d['importe'] = $value['importe'];
            $d['conferencia'] = $value['conferencia'];
            $d['email'] = $value['email'];
            $d['nombre'] = $value['nombre'] . " " . $value['apellidopaterno'] . " " . $value['apellidomaterno'];
            $d['telefonomovil'] = " " . $value['telefonomovil'];
            $d['dni_ruc'] = $value['dni_ruc'];
            $d['tipo_usuario'] = $value['tipo_usuario'];

            $formatter = new \IntlDateFormatter("es_ES", \IntlDateFormatter::LONG, \IntlDateFormatter::LONG);
            $formatter->setPattern('d MMMM Y');
           // $date = ucwords($formatter->format($value['fecharegistro']));
          //  $formatter->setPattern('h:mm a');
            //$time = ucwords($formatter->format($value['horaInicio']));
            //$d['fecharegistro'] = $date." ".$time;
           // $d['fecharegistro'] = $date;

            $formatter->setPattern('d MMMM Y');
            $date = ucwords($formatter->format($value['fechaorden']));
            $d['fechaorden'] = $date;

            $data[] = $d;

        }

        $phpExcelObject->getActiveSheet()->fromArray($data, NULL, 'A2', true);
        $phpExcelObject->getActiveSheet()->setTitle('OrdenDetalle');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        foreach (range('A', $phpExcelObject->getActiveSheet()->getHighestDataColumn()) as $col) {
            $phpExcelObject->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $phpExcelObject->getActiveSheet()->setAutoFilter('A1:L1');

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=ordenes-detalle.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    public function getOrdenDetalleByFilter($filters)
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $fields = array(
            'o.id',
            'o.metodopago',
            'o.codigo_pago',
            'o.estado_pago',
            'o.importe',
            'o.fecharegistro as fechaorden',

            'c.nombre as conferencia',
            'u.email',
            'up.nombre',
            'up.apellidomaterno',
            'up.apellidopaterno',
            'up.telefonomovil',
            'up.dni_ruc',
            'up.tipo_usuario',
        );

        $qb = $this->getQueryOrdenDetalleByFilter();
        $qb->select($fields);

        foreach ($filters as $key => $filter) {

            if ($key == "o.fecharegistro")
                $qb->andWhere($key . " like '%" . $filter . "%'");
            else
                $qb->andWhere($key . " = '" . $filter . "'");

        }

        /*$idOrderHidden = array(
            3031,
            2991,
            3469,
            3468,
            3361,
            3467,
            3483,
            3482,
            3481,
            3480,
            3479,
            3478,
            3477,
            3476,
            3475,
            3474,
            3473,
            3461,
            3121,
            3351,
            3221,
            2801,
            3191,
            1911,
            1901,
            1891,
            1881,
            1871,
            3201,
            3141,
            3131,
            3431,
            3001,
            3371,
            3211,
            3161,
            3151,
            3071,
            3061,
            3051,
            3041,
            2821,
            2751,
            2741,
            2691,
            2671,
            2661,
            2591,
            2581,
            2571,
            2561,
            2551,
            2501,
            2481,
            2471,
            2461,
            2301,
            2251,
            2231,
            2221,
            2201,
            2191,
            2181,
            2171,
            1941,
            1931,
            1811,
            1801,
            1521,
            2731,
            2651,
            2601,
            1681,
            3101,
            3091,
            3081,
            2921,
            2911,
            2881,
            2871,
            2861,
            2851,
            2841,
            2831,
            2771,
            3341,
            3281,
            3271,
            3261,
            3251,
            2981,
            2811,
            2721,
            2711,
            2701,
            2681,
            2451,
            2411,
            2371,
            2321,
            2161,
            2151,
            2141,
            2131,
            2121,
            2031,
            1971,
            1791,
            1771,
            3465
        );

        $emailHidden = array(
            "emijacobo@gmail.com"
        );

        $qb->andWhere("o.id NOT IN (".implode(', ',$idOrderHidden).")");
        $qb->andWhere("u.email NOT IN ('".implode("', '", $emailHidden)."')");
*/
        $qb->orderBy('od.id', 'DESC');
        $query = $qb->getQuery();
        //var_dump($qb->getQuery()->getSql()); die();
        return $query->getResult();
    }

    public function getQueryOrdenDetalleByFilter()
    {

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $qb
            ->from('AppBundle:Orden', 'o')
            ->innerjoin('AppBundle:OrdenDetalle', 'od', 'WITH', 'o.id = od.orden')
            ->innerjoin('AppBundle:Conferencia', 'c', 'WITH', 'c.id = od.conferencia')
            ->innerjoin('AppBundle:User', 'u', 'WITH', 'o.user = u.id')
            ->innerjoin('AppBundle:UsuarioPerfil', 'up', 'WITH', 'u.id = up.user');
        return $qb;
    }

}