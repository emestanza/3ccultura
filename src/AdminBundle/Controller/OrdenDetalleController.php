<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Orden;
use AppBundle\Entity\Conferencia;
use AppBundle\Entity\OrdenDetalle;
use AdminBundle\Form\OrdenType;

/**
 * OrdenDetalle controller.
 *
 */
class OrdenDetalleController extends Controller
{

    public function ordenDetalleAction($uid, $oid)
    {

        $em = $this->getDoctrine()->getManager();

        $orden = new Orden();
        $orden = $em->getRepository('AppBundle:Orden')->find($oid);

        if (!$orden) {
            throw $this->createNotFoundException('Unable to find Orden entity.');
        }

        $usuario = $orden->getUser();//$em->getRepository('AppBundle:User')->find($uid);
        $ordenDetalle = new OrdenDetalle();
        $usuarioPerfil = $em->getRepository('AppBundle:UsuarioPerfil')->findByUser($usuario);
        $ordenDetalle = $em->getRepository('AppBundle:OrdenDetalle')->findByOrden($orden);
        
        //$horario = $em->getRepository('AppBundle:HorarioConferencia')->findByOrden($orden);
        //$conferencia = $em->getRepository('AppBundle:Conferencia')->findByOrden($ordenDetalle->getConferencia());
        //$horario = 
        //$conferencia = $ordenDetalle->getConferencia();


        return $this->render('AdminBundle:OrdenDetalle:orden_detalle.html.twig', array(
            'orden' => $orden,
            'ordenDetalle' => $ordenDetalle,
            'usuario' => $usuario,
            'usuarioPerfil' => $usuarioPerfil[0]

        ));
    }

    // public function getOrdenDetalleByFilter($uid, $oid)
    // {

    //     $em = $this->getDoctrine()->getManager();

    //     $qb = $em->createQueryBuilder();

    //     $fields = array(
    //         'o.id',
    //         'o.fecharegistro',
    //         'o.fechavcto',
    //         'o.fechapago', 
    //         'o.metodopago', 
    //         'o.codigo_pago', 
    //         'o.estado_pago', 
    //         'o.tipo_moneda', 
    //         'o.certificado', 
    //         'o.importe',
    //         'u.importe',
    //         'o.importe',
    //         'o.importe',
    //         );

    //     $qb = $this->getQueryOrdenDetalleByFilter($uid, $oid);

    //     $qb->select($fields);
    //     $qb->orderBy('od.id', 'DESC');

    //     $query = $qb->getQuery();

    //     return $query->getResult();
    // }

// SELECT * 
// FROM orden o  
// INNER JOIN orden_detalle od ON o.id = od.orden_id
// INNER JOIN usuario u ON o.usuario_id = u.id 
// INNER JOIN usuario_perfil up ON u.id = up.usuario_id
// WHERE u.id = 21 
// AND o.id = 1511


    // public function getQueryOrdenDetalleByFilter($uid, $oid)
    // {

    //     $em = $this->getDoctrine()->getManager();

    //     $qb = $em->createQueryBuilder();

    //     $qb
    //         ->from('AppBundle:Orden', 'o')
    //         ->innerjoin('AppBundle:OrdenDetalle', 'od', 'WITH','o.id = od.orden_id')
    //         ->innerjoin('AppBundle:User', 'u', 'WITH','oo.usuario_id = u.id')
    //         ->innerjoin('AppBundle:UsuarioPerfil', 'up', 'WITH','u.id = up.usuario_id')
    //         ->where("u.id LIKE :uid")
    //         ->andWhere("o.id LIKE :oid")
    //         ->setParameters(array('uid'=>$uid,'oid'=>$oid,))
    //         ;

    //     return $qb;
    // }



}


