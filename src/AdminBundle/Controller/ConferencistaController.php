<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Conferencista;
use AdminBundle\Form\ConferencistaType;

/**
 * Conferencista controller.
 *
 */
class ConferencistaController extends Controller
{

    /**
     * Lists all Conferencista entities.
     *
     */
    public function indexAction()
    {

        return $this->render('AdminBundle:Conferencista:index.html.twig', array(
            ));

    }

    /**
     * Mostrar "Lista de Conferencistas"
     *
     */
    public function listaConferencistasAction()
    {

        $datos = '';

        $conferancias = $this->getConferencistasByFilter();

        foreach ($conferancias as $key => $value) {

            if (!empty($value['path'])) {
                $logo_html = '<span class="logo-table" style = "background-image: ';
                $logo_html .= ' url( /uploads/documents/'.$value['path'].' )">';
                $logo_html .= '</span>';
            }
            else{

                $logo_html = '<span class="logo-table" ></span>';
            }

            $estado_html = '<div class="text-center">';
            if($value['estado'] == 1){
                $estado_html .= '<small class="label bg-green">Activo</small>';
            }
            else
            {
                $estado_html .= '<small class="label bg-red">Bloqueado</small>';
            }

            $estado_html .= '</div">';

            $url_editar =  $this->generateUrl('conferencista_edit', array('id' => $value['id']));
            $url_bloquear =  $this->generateUrl('conferencista_confirm', array('id' => $value['id']));

            $accion_html = '<div class="acciones">';
            $accion_html .= ' <div class="btn-group">';
            $accion_html .= ' <a class="btn btn-default btn-sm" href="'.$url_editar.'">';
            $accion_html .= ' <i class="fa fa-edit"></i>';
            $accion_html .= ' </a>';
            $accion_html .= ' <button class="btn btn-default btn-eliminar btn-sm" data-url="'.$url_bloquear.'" data-target="#modalContent">';
            $accion_html .= ' <i class="fa fa-trash-o"></i>';
            $accion_html .= ' </button>';
            $accion_html .= ' </div>';
            $accion_html .= ' </div>';

            $data['key'] = $key+1;
            $data['imagen'] = $logo_html;
            $data['nombre'] = empty($value['nombre'])? '-----': $value['nombre'] ;
            $data['descripcion'] = empty($value['descripcion'])? '-----': $value['descripcion'] ;
            $data['estado'] = $estado_html;
            $data['accion'] = $accion_html;

            $datos[] = $data;
        }

        $respuesta = array("data"=>$datos);

        return new JsonResponse($respuesta);
    }

    public function getConferencistasByFilter()
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $fields = array(
            'c.nombre',
            'c.descripcion',
            'c.id', 
            'c.estado', 
            'f.path'
            );

        $nombre = '';
        $empresa = '';
        $estado = '';

        $qb = $this->getQueryConferencistasByFilter();

        $qb->select($fields);
        $qb->orderBy('c.id', 'DESC');

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getQueryConferencistasByFilter()
    {

        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $qb
            ->from('AppBundle:Conferencista', 'c')
            ->leftjoin('AppBundle:File', 'f', 'WITH','c.fid_img = f.id')
            ;

        return $qb;
    }

    /**
     * Creates a form to create a Conferencista entity.
     *
     * @param Conferencista $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Conferencista $entity, $securityContext, $em)
    {
        $form = $this->createForm(new ConferencistaType($securityContext, $em), $entity, array(
            'action' => $this->generateUrl('conferencista_new'),
            'method' => 'POST',
        ));

        $form->add('save', 'submit', array('label' => 'Guardar', 'attr'=>array('class'=>'btn-default')));

        return $form;
    }

    /**
     * Displays a form to create a new Conferencista entity.
     *
     */
    public function newAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');

        $entity = new Conferencista();
        $form   = $this->createCreateForm($entity, $securityContext, $em);
       
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('mensaje', 'Datos guardados correctamente!');
            
            return $this->redirect($this->generateUrl('conferencista'));

        }
        
        $logo_url = '';
        
        return $this->render('AdminBundle:Conferencista:new.html.twig', array(
            'entity' => $entity,
            'logo_url' => $logo_url,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Conferencista entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Conferencista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conferencista entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Conferencista:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Conferencista entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        
        $entity = new Conferencista();

        $entity = $em->getRepository('AppBundle:Conferencista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conferencista entity.');
        }

        $fid_logo = $entity->getFidImg();
        $logo_url = '';

        if($fid_logo){
            $foto = $em->getRepository('AppBundle:File')->find($fid_logo);
            if($foto){
                $logo_url = $foto->getWebPath();
            }
        }

        $editForm = $this->createEditForm($entity, $securityContext, $em);
       
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
    
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('mensaje', 'Datos guardados correctamente!');
            //$respuesta = $this->guardarData($request, $entity, $editForm->getName());
            //$respuesta = array('status' => 1, 'msj'=> 'Datos actualizados corectamente.' );
            // return new JsonResponse($respuesta);
            return $this->redirect($this->generateUrl('conferencia'));
        }

        return $this->render('AdminBundle:Conferencista:new.html.twig', array(
            'entity'      => $entity,
            'logo_url' => $logo_url,
            'form'   => $editForm->createView()    
        ));
    }

    /**
    * Creates a form to edit a Conferencista entity.
    *
    * @param Conferencista $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Conferencista $entity, $securityContext, $em)
    {
        $form = $this->createForm(new ConferencistaType($securityContext, $em), $entity, array(
            'action' => $this->generateUrl('conferencista_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('save', 'submit', array('label' => 'Actualizar', 'attr'=>array('class'=>'btn-default')));

        return $form;
    }

    public function confirmAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.context');
        
        $entity = new Conferencista();

        $entity = $em->getRepository('AppBundle:Conferencista')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Conferencista entity.');
        }

        $confirmForm   = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Conferencista:confirm.html.twig', array(
            'form'   => $confirmForm->createView(),
            'entity'   => $entity
        ));
    }

    /**
     * Deletes a Conferencista entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Conferencista')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Conferencista entity.');
            }

            if( $entity->getEstado()){
                $entity->setEstado(false);
            }
            else{
                $entity->setEstado(true);
            }

            $em->persist($entity);
            $em->flush();
            
            $respuesta = array('status' => 'eliminar', 'msj'=> 'Datos actualizados corectamente.' );
            return new JsonResponse($respuesta);

        }

        return $this->redirect($this->generateUrl('conferencista'));
    }

    /**
     * Creates a form to delete a Conferencista entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('conferencista_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('eliminar', 'submit', array('label' => 'Aceptar', 'attr'=>array('class'=>'btn-default')))
            ->add('cancelar', 'button', array('label' => 'Cancelar', 'attr'=>array('class'=>'btn-default')))
            ->getForm()
        ;
    }
}
