<?php

// Obtener infomacion del usuario logeado para ponerlo en la cabecera
// ej. Nombre de usuario

namespace AdminBundle\Twig;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;

class UserLoginInfo extends \Twig_Extension
{

    protected $em;
    protected $securityContext;

    public function __construct(SecurityContextInterface $securityContext, $em)
    {
        $this->em = $em;
        $this->securityContext = $securityContext;
    }

    public function getName()
    {
        return 'userlogin_info_extension';
    }

    public function getFunctions()
    {
        return array(
            'nombre_usuario' => new \Twig_Function_Method($this, 'nombreUsuario', array("is_safe" => array("html")))
        );
    }

    /**
     * Como parametro recibimos el id de usuario (tabla usuario)
     * Obtendremos el nombre del usuario segun el ROL
     *
     * @param $userId
     * @return string
     */
    public function nombreUsuario($userId)
        {

            $user = $this->securityContext->getToken()->getUser();
            $return = $user->getEmail();
            $nombre_usuario = '';

            if (true === $this->securityContext->isGranted('ROLE_USER')) {

                $repository_perfil = $this->em->getRepository('AppBundle:UsuarioPerfil');
                $perfil = $repository_perfil->findOneByUser($user);

                if ($perfil) {

                    $nombre_usuario = $perfil->getNombre();
                    $nombre_usuario .= ' '.$perfil->getApellidopaterno();

                    $return = $nombre_usuario;
                    
                }
            }
            else if (true === $this->securityContext->isGranted('ROLE_EMPRESA')){


            }
            else if (true === $this->securityContext->isGranted('ROLE_ADMIN')){


            }

            return trim($return) ? $return : $user->getEmail();
        }

}