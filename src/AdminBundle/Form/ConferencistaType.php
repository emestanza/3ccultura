<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\NotBlank;

class ConferencistaType extends AbstractType
{

    private $em;
    private $securityContext;

    public function __construct(SecurityContext $securityContext, ObjectManager $em)
    {
        $this->securityContext = $securityContext;
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('fid_img')
            ->add('descripcion','textarea', array(
                'attr' => array('cols' => '5',
                                'rows' => '15')
            ))
            ->add('estado', 'checkbox', array(
                'label'=> 'Estado',
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Conferencista'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_conferencista';
    }
}
