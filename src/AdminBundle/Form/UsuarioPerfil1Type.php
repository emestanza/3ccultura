<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\NotBlank;

class UsuarioPerfil1Type extends AbstractType
{

    private $em;
    private $securityContext;

    public function __construct(SecurityContext $securityContext, ObjectManager $em)
    {
        $this->securityContext = $securityContext;
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array(
                'label'=>'Nombre(s)',
                'required'=>true
                ))
            ->add('apellidopaterno', 'text', array(
                'label'=>'Apellido paterno',
                'required'=>false
                ))
            ->add('apellidomaterno', 'text', array(
                'label'=>'Apellido materno',
                'required'=>false
                ))
            ->add('telefonomovil', 'text', array(
                'label'=>'Teléfono móvil',
                'required'=>true
                ))
            ->add('dni_ruc', 'text', array(
                'label'=>'DNI / Carnet de extrajería',
                'required'=>true
                ))
            ->add('tipousuario', 'choice', array(
                'choices'   => array('p' => 'Persona Natural', 'e' => 'Empresa'),
                'required'  => true,
                'mapped'  => false,
                'multiple'  => false,
                'expanded'  => true,
                'label'  => false,
                'data' => 'p'
            ))
            ->add('user', new UserNewType($this->securityContext, $this->em), array('label'=>false))
        ;
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UsuarioPerfil',
            'validation_groups' => array('nuevo_usuario_perfil'),
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'usuario_perfil';
    }
}
