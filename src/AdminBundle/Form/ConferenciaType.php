<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\NotBlank;

class ConferenciaType extends AbstractType
{

    private $em;
    private $securityContext;

    public function __construct(SecurityContext $securityContext, ObjectManager $em)
    {
        $this->securityContext = $securityContext;
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array(
                'required' => true
                ))
            ->add('descripcion', 'textarea', array(
                'attr' => array('cols' => '5', 'rows' => '15'),
                'required' => true
            ))
            ->add('precio', 'text', array(
                'required' => true
                ))
            ->add('tipoMoneda', 'text', array(
                'required' => true
                ))
            ->add('fid_img')
            ->add('fecha_inicio', 'date',array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy HH:mm',
                'invalid_message' => 'Formato "Fecha inicio" invalido (DD/MM/YYYY)',
                'attr' => array('class' => 'datepicker')
            ))
            ->add('fecha_fin', 'date',array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy HH:mm',
                'invalid_message' => 'Formato "Fecha fin" invalido (DD/MM/YYYY)',
                'attr' => array('class' => 'datepicker')
            ))
            ->add('uri', 'text', array(
                'required' => true
                ))
            ->add('estado', 'checkbox', array(
                'label'=> 'Estado',
                ))
            ->add('conferencista', null, array(
                'required' => true
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Conferencia'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_conferencia';
    }
}
