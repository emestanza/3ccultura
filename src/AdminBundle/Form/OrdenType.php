<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrdenType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecharegistro')
            ->add('fechavcto')
            ->add('fechapago')
            ->add('metodopago')
            ->add('codigo_pago')
            ->add('estado_pago')
            ->add('tipo_moneda')
            ->add('importe')
            ->add('certificado')
            ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Orden'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_orden';
    }
}
