<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\NotBlank;

class UsuarioPerfilType extends AbstractType
{

    private $em;
    private $securityContext;

    public function __construct(SecurityContext $securityContext, ObjectManager $em)
    {
        $this->securityContext = $securityContext;
        $this->em = $em;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellidopaterno')
            ->add('apellidomaterno')
            ->add('telefonomovil')
            ->add('dni_ruc')
            ->add('tipo_usuario')
            ->add('fid_img')
            ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UsuarioPerfil'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'adminbundle_usuarioperfil';
    }
}
