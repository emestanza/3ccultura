(function (document, window, $) {

  $(function(){

    $("#textarea-wysihtml5").wysihtml5({
            "font-styles":  true, //Font styling, e.g. h1, h2, etc
    "color":        true, //Button to change color of font
    "emphasis":     true, //Italics, bold, etc
    "textAlign":    true, //Text align (left, right, center, justify)
    "lists":        true, //(Un)ordered lists, e.g. Bullets, Numbers
    "blockquote":   true, //Button to insert quote
    "link":         true, //Button to insert a link
    "table":        true, //Button to insert a table
    "image":        true, //Button to insert an image
    "video":        true, //Button to insert video
    "html":         true //Button which allows you to edit the generated HTML
    });

    $('.checkbox input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          increaseArea: '20%' // optional
    });

    $('.radio input').iCheck({
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });

    $('#fecharegistro').datetimepicker({
                    language: 'es',
                    pickTime: false,
                    useStrict: true
                });

    fi = $('#fechainicio');
    ff = $('#fechafin');
    fii = $('#fechainicioInput');
    fff = $('#fechafinInput');

    setdDateRange(fi, fi, ff);
    setdDateRange(ff, fi, ff);

    function setdDateRange(c, i, f)
    {

        c.click(function(){

            i.datetimepicker({
                language: 'es'
            });

            f.datetimepicker({
                language: 'es'
            });

            f.on("dp.change",function (e) {
                i.data("DateTimePicker").setMaxDate(e.date);
            });
            i.on("dp.change",function (e) {
                f.data("DateTimePicker").setMinDate(e.date);
            });

        });


    }
    
    url_upluad = $('#plupload').attr('data-url');

  upluadImg = new plupload.Uploader({
      runtimes : 'html5,flash,silverlight,html4',
      container: document.getElementById('plupload'),
      browse_button : 'browser',
      url : url_upluad,
      flash_swf_url : 'bundles/admin/js//plupload/Moxie.swf',
      silverlight_xap_url : 'bundles/admin/js//plupload/Moxie.xap',
      multi_selection:false,

      filters : {
          max_file_size : '500Kb',
          mime_types: [
              {title : "Image files", extensions : "jpg,png"},
          ]
      },

      init: {
          FilesAdded: function(up, files) {
              upluadImg.start();
              $('#display-file-progress').html('');
              $('#display-file-error').html('');
          },

          UploadProgress: function(up, file) {
              $('#display-file-progress').innerHTML = 'Subiendo: ' + file.percent + "%";
          },

          Error: function(up, err) {
              $('#display-file-error').innerHTML += "\nError #" + err.code + ": " + err.message;
          },

          FileUploaded: function(up, file, response){

              d = $.parseJSON(response.response);
              if(d.status == 1){
                  $('#fid-img').val(d.data.fid);
                  img = $('#background_image_preview');
                  img.css('background-image', 'url(' + d.data.url + ')');

                  $('#display-file-progress').html('');
                  $('#display-file-error').html('');
              }

          }
      }
  });

  upluadImg.init();

  });
})(document, window, window.jQuery);


var validarFormulario = function(e){

    if(e.attr("required") != undefined && e.attr("disabled") == undefined){

        if(e.val() != ""){
            e.parent().parent().addClass( "has-success has-feedback" );
            e.parent().parent().removeClass( "has-error has-feedback" );
            $(".icon-result", e.parent()).html("");
        }else{
            e.parent().parent().addClass( "has-error has-feedback" );
            e.parent().parent().removeClass( "has-success has-feedback" );
            $(".icon-result", e.parent()).html("");
            return false;
        }
    }
    return true;
}

function activateWidgets(b){
    b.removeClass("wait");

    b.find('[data-dismiss="modal"]').on("click", function() {
        b.modal("hide");
        b.find(".modal-content").html("");
    });

    b.modal("show");

    b.on("hidden.bs.modal", function() {
        b.find(".modal-content").html("");
        b.find('.mensaje').html("");
        b.find('.modal-dialog').removeClass('modal-sm');
        $('.bootstrap-datetimepicker-widget').removeClass('inmodal-widget');

        uploader.unbindAll();
        uploader.destroy();

    });
    b.find('.btn-cancelar').each(function() {
        b.find('.modal-dialog').addClass('modal-sm');
    });

    b.find('.btn-cancelar').on("click", function() {
        b.modal("hide");
        b.find('.modal-content').html("");
    });

    a = b.find('.mensaje').html();
    if(a != undefined){
      $.notify(a, { globalPosition:"top center", style:'bootstrap', className:'success' });
    }

    b.on('shown.bs.modal', function(){

      uploader = new plupload.Uploader({
          runtimes : 'html5,flash,silverlight,html4',
          container: document.getElementById('plupload'),
          browse_button : 'browser',
          url : "{{ path('admin_upload_file') }}",
          flash_swf_url : 'bundles/admin/js//plupload/Moxie.swf',
          silverlight_xap_url : 'bundles/admin/js//plupload/Moxie.xap',
          multi_selection:false,

          filters : {
              max_file_size : '500Kb',
              mime_types: [
                  {title : "Image files", extensions : "jpg,png"},
              ]
          },

          init: {
              FilesAdded: function(up, files) {
                  uploader.start();
                  $('#display-file-progress').html('');
                  $('#display-file-error').html('');
              },

              UploadProgress: function(up, file) {
                  b.find('#display-file-progress').innerHTML = 'Subiendo: ' + file.percent + "%";
              },

              Error: function(up, err) {
                  b.find('#display-file-error').innerHTML += "\nError #" + err.code + ": " + err.message;
              },

              FileUploaded: function(up, file, response){

                  d = $.parseJSON(response.response);
                  if(d.status == 1){
                      b.find('#fid-img').val(d.data.fid);
                      img = b.find('#background_image_preview');
                      img.css('background-image', 'url(' + d.data.url + ')');

                      b.find('#display-file-progress').html('');
                      b.find('#display-file-error').html('');
                  }

              }
          }
      });

      uploader.init();

        //b.find('#adminbundle_conferencista_descripcion').wysihtml5();
    });
    b.find('#browser').on("mouseover", function(e) {

    });
    b.find('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    });

    dateStart = '#fechainicio';
    dateEnd = '#fechafin';

    setdDate(dateStart, dateStart, dateEnd);
    setdDate(dateEnd, dateStart, dateEnd);

    function setdDate(currect, inicio, fin)
    {
        b.find(currect).on('click', function(){

            b.find(fin).datetimepicker({
                language: 'es'
                });

           b.find(inicio).datetimepicker({
                language: 'es'
                });

            b.find(fin).on("dp.change",function (e) {
               $(inicio).data("DateTimePicker").setMaxDate(e.date);
            });
            b.find(inicio).on("dp.change",function (e) {
               $(fin).data("DateTimePicker").setMinDate(e.date);
            });
            $('.bootstrap-datetimepicker-widget').addClass('inmodal-widget');

        });
    }

}